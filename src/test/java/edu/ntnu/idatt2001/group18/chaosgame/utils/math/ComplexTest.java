package edu.ntnu.idatt2001.group18.chaosgame.utils.math;

import static org.junit.jupiter.api.Assertions.assertEquals;

import edu.ntnu.idatt2001.group18.chaosgame.model.math.Complex;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class ComplexTest {

  @Nested
  public class PositiveTests {

    private Complex complex;

    @BeforeEach
    void setUp() {
      complex = new Complex(5, -12);
    }

    @Test
    @DisplayName("Tests that complex numbers compute the square root correctly.")
    void testSqrt() {
      double expectedA = 3.0;
      double expectedB = -2.0;

      Complex[] actual = complex.sqrt();

      assertEquals(expectedA, actual[0].getX0(), "These should be equal");
      assertEquals(expectedB, actual[0].getX1(), "These should be equal");
    }

    @Test
    @DisplayName("Tests that complex numbers compute the square root correctly "
        + "when the imaginary part is 0 and the real part is negative.")
    void testNegSqrtWithNoImagPart() {
      Complex complex2 = new Complex(-16, 0);
      double expectedA = 0.0;
      double expectedB = 4.0;

      Complex[] actual = complex2.sqrt();

      assertEquals(expectedA, actual[0].getX0(), "These should be equal");
      assertEquals(expectedB, actual[0].getX1(), "These should be equal");
    }
  }
}
