package edu.ntnu.idatt2001.group18.chaosgame.utils.math;

import static org.junit.jupiter.api.Assertions.assertEquals;

import edu.ntnu.idatt2001.group18.chaosgame.model.math.Matrix2x2;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.Vector2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;


class Matrix2x2Test {
  @Nested
  @DisplayName("Positive tests")
  class PositiveTests {

    Matrix2x2 matrix;
    Vector2D vector;

    @BeforeEach
    void setUp() {
      matrix = new Matrix2x2(1, 2, 3, 4);
      vector = new Vector2D(1, 2);
    }

    @Test
    @DisplayName("Test multiply")
    void testMultiply() {
      Vector2D result = matrix.multiply(vector);
      double expectedX0 = 5;
      double expectedX1 = 11;

      assertEquals(expectedX0, result.getX0());
      assertEquals(expectedX1, result.getX1());
    }
  }

  @Nested
  @DisplayName("Negative tests")
  class NegativeTests {
    Matrix2x2 matrix;
    Vector2D complex;

    @BeforeEach
    void setUp() {
      matrix = new Matrix2x2(1, 2, 3, 4);
      complex = new Vector2D(1, 2);
    }
  }
}