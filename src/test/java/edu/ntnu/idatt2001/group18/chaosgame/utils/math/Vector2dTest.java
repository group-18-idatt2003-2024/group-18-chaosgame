package edu.ntnu.idatt2001.group18.chaosgame.utils.math;

import edu.ntnu.idatt2001.group18.chaosgame.model.math.Complex;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.Vector2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class Vector2dTest {

  @Nested
  @DisplayName("Positive tests")
  public class PositiveTests {

    Vector2D vector1;
    Vector2D vector2;

    @BeforeEach
    void setUp() {
      // Create test vectors
      vector1 = new Vector2D(3, 5);
      vector2 = new Vector2D(4, 6);
    }

    @Test
    @DisplayName("Tests that vectors add correctly")
    void testAdd() {
      Vector2D expected = new Vector2D(7, 11);
      Vector2D actual = vector1.add(vector2);

      assertEquals(expected.getX0(), actual.getX0(), "These should be equal");
      assertEquals(expected.getX1(), actual.getX1(), "These should be equal");
    }

    @Test
    @DisplayName("Tests that vectors get x0 correctly")
    void testGetX0() {
      double expected = 3;
      double actual = vector1.getX0();

      assertEquals(expected, actual, "These should be equal");
    }

    @Test
    @DisplayName("Tests that vectors get x1 correctly")
    void testGetX1() {
      double expected = 6;
      double actual = vector2.getX1();

      assertEquals(expected, actual, "These should be equal");
    }

    @Test
    @DisplayName("Tests that vectors scale correctly")
    void testScale() {
      Vector2D expected = new Vector2D(7.5, 12.5);
      Vector2D actual = vector1.scale(2.5);

      assertEquals(expected.getX0(), actual.getX0(), "These should be equal");
      assertEquals(expected.getX1(), actual.getX1(), "These should be equal");
    }

    @Test
    @DisplayName("Tests that vectors subtracts correctly")
    void testSubtract() {
      Vector2D expected = new Vector2D(-1, -1);
      Vector2D actual = vector1.subtract(vector2);

      assertEquals(expected.getX0(), actual.getX0(), "These should be equal");
      assertEquals(expected.getX1(), actual.getX1(), "These should be equal");
    }
  }

  @Nested
  @DisplayName("Negative tests")
  public class NegativeTest {

    Vector2D vector1;
    Complex complex;

    @BeforeEach
    void setUp() {
      vector1 = new Vector2D(3, 5);
      complex = new Complex(4, 6);

    }

    @Test
    @DisplayName("Test that subtracting a complex from a vector throws an IllegalArgumentException")
    void subtractComplexFromVectorTest() {
      Exception e = assertThrows(IllegalArgumentException.class, () -> vector1.subtract(complex));
      assertEquals(
          String.format(
              "The vectors must be of the same type. Cannot subtract '%s' from '%s'%n",
              complex.getClass().getSimpleName(),
              vector1.getClass().getSimpleName()
          ),
          e.getMessage(),
          "These should be equal"
      );
    }
  }
}
