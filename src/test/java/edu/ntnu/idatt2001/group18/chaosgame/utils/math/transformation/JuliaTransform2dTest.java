package edu.ntnu.idatt2001.group18.chaosgame.utils.math.transformation;

import edu.ntnu.idatt2001.group18.chaosgame.model.math.Complex;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.Vector2D;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation.JuliaTransform;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class JuliaTransform2dTest {

  @Nested
  @DisplayName("Positive tests")
  class PositiveTests {
    @Test
    @DisplayName("Test transform")
    void testTransform() {
      Complex point1 = new Complex(1, 1);
      JuliaTransform juliaTransform = new JuliaTransform(point1, 1);

      Complex point2 = new Complex(6, -11);
      Vector2D result = juliaTransform.transform(point2);

      assertEquals(-84, result.getX0());
      assertEquals(-131, result.getX1());
    }
  }

  @Nested
  @DisplayName("Negative tests")
  class NegativeTests {

    Complex complex;
    Vector2D vector;
    JuliaTransform juliaTransform;

    @BeforeEach
    void setUp() {
      complex = new Complex(1, 1);
      juliaTransform = new JuliaTransform(complex, 1);
      vector = new Vector2D(1, 1);
    }

    @Test
    @DisplayName("Constructor with invalid sign")
    void testConstructorWithInvalidSign() {
      var exception = assertThrows(AssertionError.class, () -> new JuliaTransform(complex, 0));

      assertEquals("Sign must be 1 or -1", exception.getMessage());
    }
  }
}
