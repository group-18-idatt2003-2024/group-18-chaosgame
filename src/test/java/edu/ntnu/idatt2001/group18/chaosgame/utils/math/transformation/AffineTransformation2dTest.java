package edu.ntnu.idatt2001.group18.chaosgame.utils.math.transformation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import edu.ntnu.idatt2001.group18.chaosgame.model.math.Matrix2x2;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.Vector2D;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation.AffineTransform2D;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * <h1>
 * AffineTransformation2dTest
 * </h1>
 * AffineTransformation2dTest.
 *
 * <p>
 * Tests for the AffineTransform2D-class.
 * </p>
 */
public class AffineTransformation2dTest {
  /**
   * <h2>
   * PositiveTests
   * </h2>
   * PositiveTests.
   *
   * <p>
   * This class encapsulates all positive tests, e.g. tests that when input is valid the output
   * is valid.
   * </p>
   */
  @Nested
  public class PositiveTests {

    private AffineTransform2D affineTransform2D;

    @BeforeEach
    void setUp() {
      affineTransform2D = new AffineTransform2D(
          new Matrix2x2(3, 2, 6, 3),
          new Vector2D(3, 5));
    }

    @Test
    @DisplayName("Tests that affine transformations transform correctly")
    void testTransform() {
      Vector2D vector = new Vector2D(6, 2);
      Vector2D expected = new Vector2D(25, 47);
      Vector2D actual = affineTransform2D.transform(vector);

      assertEquals(expected.getX0(), actual.getX0(), "These should be equal");
      assertEquals(expected.getX1(), actual.getX1(), "These should be equal");
    }
  }
}
