package edu.ntnu.idatt2001.group18.chaosgame.view.gui.sidebar.settings;

import edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation.AffineTransform2D;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation.JuliaTransform;
import edu.ntnu.idatt2001.group18.chaosgame.utils.ChaosGameFileHandler;
import edu.ntnu.idatt2001.group18.chaosgame.utils.Constants;
import edu.ntnu.idatt2001.group18.chaosgame.utils.ObserverMessage;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.Observable;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.Observer;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.sidebar.settings.editors.*;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.effect.InnerShadow;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class FilesView extends VBox implements Observable, Observer {
  private final ArrayList<Observer> observers = new ArrayList<>();
  private final Map<String, BaseTransformationEditor> editors = new HashMap<>();
  private ListView<String> listView;
  private ObservableList<String> fileList;
  private ScrollPane editorPane;
  private Button saveButton;
  private TransformationEditor currentEditor;
  private VBox editorContainer = new VBox();
  private File selectedFile;

  public FilesView() {
    super(4);
    initializeComponents();
    setupFileClickHandler();

    setPadding(new Insets(20, 0, 40, 0));
  }

  private void initializeComponents() {
    initializeEditors();
    initializeTransformationsHeader();
    initializeListView();
    initializeEditorPane();
  }

  private Button styledButton(String text, String color, String hoverColor) {
    Button button = new Button(text);
    button.setPrefWidth(80);
    button.setStyle("-fx-background-color: " + color + "; -fx-text-fill: white; -fx-font-weight: bold;"
        + " -fx-effect: dropshadow(three-pass-box, rgba(0.2,0.2,0.2,0.3), 5, 0, 0, 2);");
    button.setOnMouseEntered(e -> button.setStyle("-fx-cursor: hand; -fx-scale-x: 1.02; -fx-scale-y: 1.02;"
        + "-fx-background-color: " + hoverColor + "; -fx-text-fill: white; -fx-font-weight: bold; "
        + "-fx-effect: " + "dropshadow(three-pass-box, rgba(0.2,0.2,0.2,0.3), 5, 0, 0, 2);"));
    button.setOnMouseExited(e -> button.setStyle("-fx-background-color: " + color + "; -fx-text-fill: white; "
        + "-fx-font-weight: bold; -fx-effect: dropshadow(three-pass-box, rgba(0.2,0.2,0.2,0.3), "
        + "5, 0, 0, 2);"));
    return button;
  }

  private Button newTransformationButton() {
    Button newTransformationButton = styledButton("New", "#9999ee", "#aaaaff");
    newTransformationButton.setOnAction(e -> createNewTransformation());
    return newTransformationButton;
  }

  private Button editTransformationButton() {
    Button editTransformationButton = styledButton("Edit", "#9999ee", "#aaaaff");
    editTransformationButton.setOnAction(e -> {
      if (selectedFile != null) {
        notifyObservers(ObserverMessage.LOAD_FILE, new Object[]{selectedFile});
      }
    });
    return editTransformationButton;
  }

  private Button deleteTransformationButton() {
    Button deleteTransformationButton = styledButton("Delete", "#bb2222", "#cd3333");
    deleteTransformationButton.setOnAction(e -> {
      if (selectedFile != null) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to delete this transformation?");
        alert.showAndWait().ifPresent(response -> {
          if (response == ButtonType.OK) {
            try {
              Files.delete(selectedFile.toPath());
              populateList();
              toggleEditorPaneVisibility(false);
            } catch (IOException ex) {
              Alert errorAlert = new Alert(Alert.AlertType.ERROR, "Could not delete file: " + ex.getMessage(), ButtonType.OK);
              errorAlert.showAndWait();
            }
          }
        });
      }
    });
    return deleteTransformationButton;
  }

  private void createNewTransformation() {
    TextInputDialog dialog = new TextInputDialog("new_transformation");
    dialog.setTitle("Create New Transformation");
    dialog.setHeaderText("Create a new transformation file");
    dialog.setContentText("Please enter the file name:");

    Optional<String> result = dialog.showAndWait();
    result.ifPresent(name -> {
      File file = new File(Constants.FRACTAL_FOLDER_PATH, name + ".txt");
      if (!file.exists()) {
        try {
          boolean created = file.createNewFile();
          if (created) {
            populateList(); // Refresh the list
            listView.getSelectionModel().select(name);
            selectedFile = file;
            setEditorType("AffineTransform2D");  // Default to affine or based on user selection
            currentEditor.loadTransformation("AffineTransform2D");  // Load empty data into editor
            toggleEditorPaneVisibility(true);
          }
        } catch (IOException ex) {
          Alert alert = new Alert(Alert.AlertType.ERROR, "Could not create file: " + ex.getMessage(), ButtonType.OK);
          alert.showAndWait();
        }
      } else {
        Alert alert = new Alert(Alert.AlertType.WARNING, "File already exists.", ButtonType.OK);
        alert.showAndWait();
      }
    });
  }


  public void initializeEditors() {
    editors.clear();

    editors.put(AffineTransform2D.class.getSimpleName(), new AffineTransformationEditor());
    editors.put(JuliaTransform.class.getSimpleName(), new JuliaTransformationEditor());

    editors.get(AffineTransform2D.class.getSimpleName()).getTypeSelector().addObserver(this);
    editors.get(JuliaTransform.class.getSimpleName()).getTypeSelector().addObserver(this);
  }

  private void setEditorType(String type) {
    currentEditor = editors.getOrDefault(type, editors.get(AffineTransform2D.class.getSimpleName()));
    editorContainer.getChildren().clear();
    editorContainer.getChildren().add(currentEditor.getView());
  }

  private void initializeTransformationsHeader() {
    Label headerLabel = new Label("Transformations");
    headerLabel.setStyle("-fx-font-size: 22px; -fx-font-weight: bold;");
    headerLabel.setPadding(new Insets(20, 0, 8, 0));

    BorderPane header = new BorderPane();
    header.setLeft(newTransformationButton());
    header.setCenter(editTransformationButton());
    header.setRight(deleteTransformationButton());

    VBox headerContainer = new VBox(headerLabel, header);
    headerContainer.setPadding(new Insets(0, 0, 12, 0));

    this.getChildren().add(headerContainer);
  }

  private void initializeListView() {
    listView = new ListView<>();
    fileList = FXCollections.observableArrayList();
    listView.setMinWidth(260);
    listView.setStyle("-fx-cursor: hand; -fx-stroke: #9999ee; -fx-stroke-width: 0px; -fx-background-color: #f0f0f0; "
        + "-fx-effect: dropshadow(three-pass-box, rgba(0.2,0.2,0.2,0.2), 5, 0, 0, 2);");
    listView.setItems(fileList);
    this.getChildren().add(listView);
    populateList();
    fileList.addListener((ListChangeListener<String>) change -> adjustListViewHeight());
    adjustListViewHeight();
  }

  private void initializeEditorPane() {
    editorPane = new ScrollPane();
    editorPane.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
    editorContainer = new VBox();  // This will hold the specific editor view
    // Set the background with rounded corners
    BackgroundFill fill = new BackgroundFill(Color.web("#EBEBEB"), new CornerRadii(4), Insets.EMPTY);
    editorContainer.setBackground(new Background(fill));

    // Set an inner shadow
    InnerShadow innerShadow = new InnerShadow();
    innerShadow.setOffsetX(1);
    innerShadow.setOffsetY(0.5);
    innerShadow.setColor(Color.LIGHTGRAY);
    editorContainer.setEffect(innerShadow);
    editorPane.setContent(editorContainer);
    editorPane.prefWidthProperty().bind(listView.widthProperty());
    editorPane.setMaxHeight(400);
    editorContainer.prefWidthProperty().bind(editorPane.widthProperty().divide(1.07));
    editorContainer.setPadding(new Insets(0, 4, 0, 0));

    saveButton = new Button("Save");
    saveButton.prefWidthProperty().bind(editorPane.widthProperty());
    saveButton.setStyle("-fx-background-color: #44aa44; -fx-text-fill: white; -fx-font-weight: bold;"
        + " -fx-effect: dropshadow(three-pass-box, rgba(0.2,0.2,0.2,0.3), 5, 0, 0, 2);");
    saveButton.setOnMouseEntered(e -> saveButton.setStyle("-fx-cursor: hand; -fx-scale-x: 1.02; -fx-scale-y: 1.02;"
        + "-fx-background-color: #55bb55; -fx-text-fill: white; -fx-font-weight: bold; "
        + "-fx-effect: " + "dropshadow(three-pass-box, rgba(0.2,0.2,0.2,0.3), 5, 0, 0, 2);"));
    saveButton.setOnMouseExited(e -> saveButton.setStyle("-fx-background-color: #44aa44; -fx-text-fill: white; "
        + "-fx-font-weight: bold; -fx-effect: dropshadow(three-pass-box, rgba(0.2,0.2,0.2,0.3), "
        + "5, 0, 0, 2);"));
    saveButton.setOnAction(event -> {
      if (currentEditor != null) {
        notifyObservers(ObserverMessage.SAVE_FILE, new Object[]{currentEditor.saveTransformation(), selectedFile});
        toggleEditorPaneVisibility(false);
      }
    });

    this.getChildren().add(editorPane);
    this.getChildren().add(saveButton);
    toggleEditorPaneVisibility(false);
  }


  public void updateEditor(String content, File file) {
    String type = ChaosGameFileHandler.getTransformationType(file);
    setEditorType(type);
    currentEditor.loadTransformation(content);
    selectedFile = file;
    toggleEditorPaneVisibility(true);
  }

  public void handleFileLoadError(String message) {
    Alert fileLoadAlert = new Alert(Alert.AlertType.ERROR, "Error loading file: " + message);
    fileLoadAlert.showAndWait();
    toggleEditorPaneVisibility(true);
  }

  private void setupFileClickHandler() {
    listView.setOnMouseClicked(event -> {
      if (fileList.isEmpty()) {
        return;
      }
      selectedFile = new File(
          Constants.FRACTAL_FOLDER_PATH, listView.getSelectionModel().getSelectedItem() + ".txt");
      notifyObservers(ObserverMessage.SELECT_GAME, new Object[]{selectedFile});
      if (event.getClickCount() == 2) {
        notifyObservers(ObserverMessage.LOAD_FILE, new Object[]{selectedFile});
      }
    });
  }

  private void adjustListViewHeight() {
    final int maxVisibleItems = 10;
    final double itemHeight = 24.0;
    int itemsCount = Math.min(fileList.size(), maxVisibleItems);
    listView.setPrefHeight(itemsCount * itemHeight + 2);
    listView.setMaxHeight(itemsCount * itemHeight + 2);
    listView.setMinHeight(3 * itemHeight);
  }

  private void populateList() {
    Path dirPath = Paths.get(Constants.FRACTAL_FOLDER_PATH);
    try {
      fileList.setAll(Files.list(dirPath)
          .filter(Files::isRegularFile)
          .map(Path::getFileName)
          .map(Path::toString)
          .filter(name -> name.endsWith(".txt"))
          .map(name -> name.replace(".txt", ""))
          .sorted()
          .collect(Collectors.toList()));
    } catch (IOException e) {
      System.err.printf("Error: %s. Failed to read directory: %s%n", e.getMessage(), dirPath);
    }
  }

  @Override
  public ArrayList<Observer> getObservers() {
    return observers;
  }

  @Override
  public void addObserver(Observer observer) {
    if (!getObservers().contains(observer)) {
      getObservers().add(observer);
      notifyObservers(ObserverMessage.SELECT_GAME, new Object[]{new File(Constants.FRACTAL_FOLDER_PATH + "julia.txt")});
    }
  }

  @Override
  public void removeObserver(Observer observer) {
    getObservers().remove(observer);
  }

  @Override
  public void notifyObservers(ObserverMessage message, Object[] data) {
    getObservers().forEach(observer -> observer.update(message, data));
  }

  public void toggleEditorPaneVisibility(boolean visible) {
    editorPane.setVisible(visible);
    editorPane.setManaged(visible);
    saveButton.setVisible(visible);
    saveButton.setManaged(visible);
  }

  @Override
  public void update(ObserverMessage message, Object[] data) {
    if (message.equals(ObserverMessage.CHANGE_EDITOR)) {
      setEditorType(((String) data[0]).trim());
    }
  }
}
