package edu.ntnu.idatt2001.group18.chaosgame.view.gui.sidebar.settings;

import edu.ntnu.idatt2001.group18.chaosgame.utils.ObserverMessage;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.Observable;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.Observer;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CreateCanvasView extends GridPane implements Observable {
  private final ArrayList<Observer> observers = new ArrayList<>();
  private final TextField sizeField = new TextField();
  private final Button createCanvasButton = new Button("Create Canvas");

  /**
   * <h2>Constructor</h2>
   * Constructor.
   *
   * <p>
   * Constructor for the CreateCanvasView class. This class is the view
   * for the create canvas settings. It contains a text field for the size
   * of the canvas as well as a button to create the canvas.
   * </p>
   */
  public CreateCanvasView() {
    super();

    setHgap(10);


    Label sizeLabel = new Label("Size:");
    sizeLabel.setStyle("-fx-font-weight: bold;");
    setupSizeField();
    setupCreateCanvasButton();

    prefWidthProperty().setValue(Double.MAX_VALUE);
    sizeLabel.setMinWidth(26);
    sizeField.setMaxWidth(104);
    createCanvasButton.setMinWidth(106);

    createCanvasButton.setStyle("-fx-background-color: #9999ee; -fx-text-fill: white; -fx-font-weight: bold;"
        + " -fx-effect: dropshadow(three-pass-box, rgba(0.2,0.2,0.2,0.3), 5, 0, 0, 2);");
    createCanvasButton.setOnMouseEntered(e -> createCanvasButton.setStyle("-fx-cursor: hand; -fx-scale-x: 1.02; -fx-scale-y: 1.02;"
        + "-fx-background-color: #aaaaff; -fx-text-fill: white; -fx-font-weight: bold; "
        + "-fx-effect: " + "dropshadow(three-pass-box, rgba(0.2,0.2,0.2,0.3), 5, 0, 0, 2);"));
    createCanvasButton.setOnMouseExited(e -> createCanvasButton.setStyle("-fx-background-color: #9999ee; -fx-text-fill: white; "
        + "-fx-font-weight: bold; -fx-effect: dropshadow(three-pass-box, rgba(0.2,0.2,0.2,0.3), "
        + "5, 0, 0, 2);"));

    addRow(0, sizeLabel, sizeField, createCanvasButton);
  }

  public TextField getSizeField() {
    return sizeField;
  }

  public Button getCreateCanvasButton() {
    return createCanvasButton;
  }

  public void toggleCanvasSizeVisibility(boolean visible) {
    getSizeField().setDisable(!visible);
  }

  private void setupSizeField() {
    getSizeField().setText("500");

    // Limit the size field to 10000
    getSizeField().textProperty().addListener((observable, oldValue, newValue) -> {
      if (isInteger(newValue)) {
        if (Integer.parseInt(newValue) > 5_000) {
          getSizeField().setText("5000");
        }
      } else if (!newValue.isBlank()) {
        getSizeField().setText(oldValue);
      }
    });
  }

  private void setupCreateCanvasButton() {
    // Notify the observers when the create canvas button is clicked
    getCreateCanvasButton().setOnAction(e -> notifyObservers(ObserverMessage.NEW_CANVAS, new Object[]{getSize()}));
  }

  private Integer getSize() {
    if (getSizeField().getText().isBlank() || Integer.parseInt(getSizeField().getText()) < 500) {
      getSizeField().setText("500");
    }
    return Integer.parseInt(getSizeField().getText());
  }

  private boolean isInteger(String input) {
    String regex = "^-?\\d+$";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(input);
    return matcher.matches();
  }

  @Override
  public ArrayList<Observer> getObservers() {
    return observers;
  }

  @Override
  public void addObserver(Observer observer) {
    getObservers().add(observer);
  }

  @Override
  public void removeObserver(Observer observer) {
    getObservers().remove(observer);
  }

  @Override
  public void notifyObservers(ObserverMessage message, Object[] data) {
    getObservers().forEach(o -> o.update(message, data));
  }
}
