package edu.ntnu.idatt2001.group18.chaosgame.view.gui.sidebar.settings.editors;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;

/**
 * <h1>BaseTransformationEditor</h1>
 * BaseTransformationEditor.
 *
 * <p>
 *   This class is a base class for editing transformations.
 * </p>
 */
public abstract class BaseTransformationEditor implements TransformationEditor {
  protected GridPane view = new GridPane();
  protected TextField minCoordX = new TextField();
  protected TextField minCoordY = new TextField();
  protected TextField maxCoordX = new TextField();
  protected TextField maxCoordY = new TextField();
  protected TransformationTypeSelector typeSelector;

  public BaseTransformationEditor(String transformationType) {
    typeSelector = new TransformationTypeSelector(transformationType);
    setupCoordinateFields();
  }

  public TransformationTypeSelector getTypeSelector() {
    return typeSelector;
  }

  protected void setupCoordinateFields() {
    view.setHgap(10);  // Horizontal gap
    view.setVgap(5);   // Vertical gap
    view.setPadding(new Insets(7));  // Padding around the grid

    // Define column constraints to equally divide the space
    ColumnConstraints column = new ColumnConstraints();
    column.setPercentWidth(25); // Each column takes 25% of the width
    column.setHgrow(Priority.ALWAYS); // Allow columns to grow

    // Apply the same column constraints to all four columns
    view.getColumnConstraints().addAll(column, column, column, column);

    Label typeTitle = new Label("Transformation type:");
    typeTitle.setStyle("-fx-font-weight: bold");
    GridPane.setColumnSpan(typeTitle, 4);  // Span across all columns
    view.add(typeTitle, 0, 0);

    // First row for the type selector, spanning all columns
    HBox typeSelectorBox = new HBox(typeSelector.getView());
    typeSelectorBox.setSpacing(10);  // Set spacing within the box
    GridPane.setColumnSpan(typeSelectorBox, 4);  // Span across all columns
    view.add(typeSelectorBox, 0, 1);  // Add to first row

    Label coordinateTitle = new Label("Coordinates:");
    coordinateTitle.setStyle("-fx-font-weight: bold");
    coordinateTitle.setPadding(new Insets(20, 0, 0, 0));
    GridPane.setColumnSpan(coordinateTitle, 4);  // Span across all columns
    view.add(coordinateTitle, 0, 2);

    setupRevertOnInvalidInput(minCoordX);
    setupRevertOnInvalidInput(minCoordY);
    setupRevertOnInvalidInput(maxCoordX);

    maxCoordY.setDisable(true);
    maxCoordY.setText("auto");

    // Setup for min and max coordinate inputs
    view.addRow(3, new Label("Min X:"), minCoordX, new Label("Min Y:"), minCoordY);
    view.addRow(4, new Label("Max X:"), maxCoordX, new Label("Max Y:"), maxCoordY);
  }

  protected void setupRevertOnInvalidInput(TextField textField) {
    // Allows optional minus, numbers, and a decimal part
    textField.textProperty().addListener((observable, oldValue, newValue) -> {
      if (!newValue.matches("-?\\d*([.]\\d*)?")) {  // Check against the regex
        textField.setText(oldValue);  // Revert to the old (valid) value
      }
    });
  }


  @Override
  public Node getView() {
    return view;
  }

  protected void loadCoordinates(String minCoords, String maxCoords) {
    String[] min = minCoords.split(",");
    String[] max = maxCoords.split(",");
    minCoordX.setText(min[0]);
    minCoordY.setText(min[1]);
    maxCoordX.setText(max[0]);
  }

  protected String getCoordinateString() {
    if (minCoordX.getText().isEmpty()) {
      minCoordX.setText("0");
    }
    if (minCoordY.getText().isEmpty()) {
      minCoordY.setText("0");
    }
    if (maxCoordX.getText().isEmpty()) {
      maxCoordX.setText("0");
    }

    return minCoordX.getText() + "," + minCoordY.getText() + "\n"
        + maxCoordX.getText() + ",0";
  }
}
