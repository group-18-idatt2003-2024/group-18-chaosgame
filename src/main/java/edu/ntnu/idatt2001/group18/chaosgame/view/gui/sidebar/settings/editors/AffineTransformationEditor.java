package edu.ntnu.idatt2001.group18.chaosgame.view.gui.sidebar.settings.editors;

import edu.ntnu.idatt2001.group18.chaosgame.model.math.Matrix2x2;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.Vector2D;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation.AffineTransform2D;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

/**
 * <h1>
 *  AffineTransformationEditor
 *  </h1>
 *  AffineTransformationEditor.
 *
 *  <p>
 *    This class is a view for editing affine transformations.
 *    It extends the BaseTransformationEditor class.
 *  </p>
 */
public class AffineTransformationEditor extends BaseTransformationEditor {
  private final List<AffineTransform2D> affineTransformations = new ArrayList<>();
  private final List<TextField[]> matrixFields = new ArrayList<>();
  private final List<TextField[]> vectorFields = new ArrayList<>();

  public AffineTransformationEditor() {
    super(AffineTransform2D.class.getSimpleName());
  }

  private void renderAffineSpecificControls() {
    view.getChildren().clear(); // Clear existing controls before re-rendering
    matrixFields.clear();
    vectorFields.clear();
    setupCoordinateFields();
    setupColumnConstraints();

    for (int i = 0; i < affineTransformations.size(); i++) {
      Label matrixLabel = new Label("Matrix " + (i + 1) + ":");
      matrixLabel.setPadding(new Insets(20, 0, 0, 0));
      matrixLabel.setStyle("-fx-font-weight: bold");
      GridPane.setColumnSpan(matrixLabel, 2);  // Span across all columns

      TextField a0 = new TextField();
      TextField a1 = new TextField();
      setupRevertOnInvalidInput(a0);
      setupRevertOnInvalidInput(a1);

      TextField b0 = new TextField();
      TextField b1 = new TextField();
      setupRevertOnInvalidInput(b0);
      setupRevertOnInvalidInput(b1);


      matrixFields.add(new TextField[]{a0, a1, b0, b1});

      Label vectorLabel = new Label("Vector " + (i + 1) + ":");
      vectorLabel.setStyle("-fx-font-weight: bold");
      vectorLabel.setPadding(new Insets(20, 0, 0, 0));
      GridPane.setColumnSpan(vectorLabel, 2);

      TextField x = new TextField();
      TextField y = new TextField();

      setupRevertOnInvalidInput(x);
      setupRevertOnInvalidInput(y);

      vectorFields.add(new TextField[]{x, y});

      Button deleteButton = new Button("Delete");
      GridPane.setColumnSpan(deleteButton, 2);
      int finalI = i;
      deleteButton.setOnAction(e -> {
        if (affineTransformations.size() <= 1) {
          return;
        }
        affineTransformations.remove(finalI);
        renderAffineSpecificControls();
        populateFields();
      });

      int rowIndex = 6 + i * 4;
      view.addRow(rowIndex, matrixLabel, new Label(""), new Label(""), vectorLabel);
      view.addRow(rowIndex + 1, a0, a1, new Label(""), x);
      view.addRow(rowIndex + 2, b0, b1, new Label(""), y);
      view.addRow(rowIndex + 3, deleteButton);
    }
    Button addButton = new Button("Add new transformation");
    addButton.setOnAction(e -> {
      affineTransformations.add(
          new AffineTransform2D(
              new Matrix2x2(
                  0, 0, 0, 0
              ),
              new Vector2D(0, 0)
          )
      );
      renderAffineSpecificControls();
      populateFields();
    });
    GridPane.setColumnSpan(addButton, 4);
    view.addRow(7 + affineTransformations.size() * 4, addButton);
  }

  private void setupColumnConstraints() {
    view.getColumnConstraints().clear();
    for (int i = 0; i < 4; i++) {
      ColumnConstraints column = new ColumnConstraints();
      column.setPercentWidth(25); // Each column takes 25% of the width
      column.setHgrow(Priority.ALWAYS); // Allow columns to grow
      view.getColumnConstraints().add(column);
    }
  }

  @Override
  public void loadTransformation(String data) {
    String[] lines = data.split("\n");
    if (lines.length >= 3) {
      this.typeSelector.setType(AffineTransform2D.class.getSimpleName());
      super.loadCoordinates(lines[1], lines[2]);
    }
    affineTransformations.clear();  // Clear any existing transformations
    for (int i = 3; i < lines.length; i++) {
      double[] lineParts = Arrays
          .stream(lines[i].split(","))
          .mapToDouble(Double::parseDouble)
          .toArray();
      if (lineParts.length < 6) {
        continue;
      }
      Matrix2x2 matrix2x2 = new Matrix2x2(lineParts[0], lineParts[1], lineParts[2], lineParts[3]);
      Vector2D vector2D = new Vector2D(lineParts[4], lineParts[5]);
      affineTransformations.add(new AffineTransform2D(matrix2x2, vector2D));
    }
    renderAffineSpecificControls();
    populateFields();
  }

  private void populateFields() {
    for (int i = 0; i < affineTransformations.size() && i < matrixFields.size(); i++) {
      AffineTransform2D transformation = affineTransformations.get(i);
      TextField[] fields = matrixFields.get(i);
      fields[0].setText(String.valueOf(transformation.getMatrix().getA00()));
      fields[1].setText(String.valueOf(transformation.getMatrix().getA01()));
      fields[2].setText(String.valueOf(transformation.getMatrix().getA10()));
      fields[3].setText(String.valueOf(transformation.getMatrix().getA11()));
    }
    for (int i = 0; i < affineTransformations.size() && i < vectorFields.size(); i++) {
      AffineTransform2D transformation = affineTransformations.get(i);
      TextField[] fields = vectorFields.get(i);
      fields[0].setText(String.valueOf(transformation.getVector().getX0()));
      fields[1].setText(String.valueOf(transformation.getVector().getX1()));
    }
  }

  @Override
  public String saveTransformation() {
    StringBuilder sb = new StringBuilder();
    // set all empty fields to 0
    for (int i = 0; i < matrixFields.size(); i++) {
      TextField[] matrix = matrixFields.get(i);
      TextField[] vector = vectorFields.get(i);
      for (TextField field : matrix) {
        if (field.getText().isEmpty()) {
          field.setText("0");
        }
      }
      for (TextField field : vector) {
        if (field.getText().isEmpty()) {
          field.setText("0");
        }
      }
    }

    sb.append(AffineTransform2D.class.getSimpleName())
        .append("\n")
        .append(super.getCoordinateString())
        .append("\n");
    for (int i = 0; i < matrixFields.size() && i < vectorFields.size(); i++) {
      TextField[] matrix = matrixFields.get(i);
      TextField[] vector = vectorFields.get(i);
      sb.append(matrix[0].getText()).append(",")
          .append(matrix[1].getText()).append(",")
          .append(matrix[2].getText()).append(",")
          .append(matrix[3].getText()).append(",")
          .append(vector[0].getText()).append(",")
          .append(vector[1].getText()).append("\n");
    }
    return sb.toString();
  }
}
