package edu.ntnu.idatt2001.group18.chaosgame.view.gui.center.canvas;

import edu.ntnu.idatt2001.group18.chaosgame.model.math.Vector2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

/**
 * <h1>ChaosGameCanvas</h1>
 * ChaosGameCanvas.
 *
 * <p>
 * This class is an abstract class for the canvas of the chaos game.
 * It is responsible for displaying the canvas.
 * </p>
 */
public abstract class ChaosGameCanvas extends Canvas {
  private double lastMousePosX;
  private double lastMousePosY;
  private Vector2D topLeftCoords;
  private Vector2D bottomRightCoords;

  /**
   * <h2>Constructor</h2>
   * Constructor.
   *
   * <p>
   * Constructor for the ChaosGameCanvas class.
   * </p>
   *
   * @param width             The width of the canvas.
   * @param height            The height of the canvas.
   * @param topLeftCoords     The top left plane coordinates of the canvas.
   * @param bottomRightCoords The bottom right plane coordinates of the canvas.
   */
  public ChaosGameCanvas(
      int width,
      int height,
      Vector2D topLeftCoords,
      Vector2D bottomRightCoords) {
    setTopLeftCoords(topLeftCoords);
    setBottomRightCoords(bottomRightCoords);

    this.setWidth(width);
    this.setHeight(height);

    // Set a zoom that fits the window
    this.setScaleX(1000 / getWidth());
    this.setScaleY(1000 / getHeight());

    this.setOnMousePressed(this::onMousePressed);
    this.setOnMouseDragged(this::onMouseDragged);
    this.setOnMouseReleased(this::onMouseRelease);

    // Draw a border around the canvas
    double borderWidth = this.getWidth() / 200.0;
    this.getGraphicsContext2D().setStroke(Color.BLACK);
    this.getGraphicsContext2D().setLineWidth(borderWidth);

    // Draw a border around the canvas
    this.getGraphicsContext2D().strokeRect(
        borderWidth / 2,
        borderWidth / 2,
        this.getWidth() - borderWidth,
        this.getHeight() - borderWidth
    );
  }

  /**
   * <h2>createCanvasForAffineGame</h2>
   * createCanvasForAffineGame.
   *
   * <p>
   * This method creates a canvas for the affine game.
   * </p>
   *
   * @param minCoords        The minimum plane coordinates of the canvas.
   * @param maxCoords        The maximum plane coordinates of the canvas.
   * @param parentScrollPane The parent scroll pane (needed for panning).
   * @param width            The width of the canvas.
   * @param height           The height of the canvas.
   * @return A canvas for the affine game.
   */
  public static AffineCanvas createCanvasForAffineGame(Vector2D minCoords,
                                                       Vector2D maxCoords,
                                                       ScrollPane parentScrollPane,
                                                       int width,
                                                       int height) {
    Vector2D topLeftCoords = new Vector2D(minCoords.getX0(), maxCoords.getX1());
    Vector2D bottomRightCoords = new Vector2D(maxCoords.getX0(), minCoords.getX1());

    return new AffineCanvas(
        width,
        height,
        topLeftCoords,
        bottomRightCoords,
        parentScrollPane
    );
  }

  /**
   * <h2>createCanvasForJuliaGame</h2>
   * createCanvasForJuliaGame.
   *
   * <p>
   * This method creates a canvas for the Julia game.
   * </p>
   *
   * @param minCoords The minimum plane coordinates of the canvas.
   * @param maxCoords The maximum plane coordinates of the canvas.
   * @return A canvas for the Julia game.
   */
  public static JuliaCanvas createCanvasForJuliaGame(Vector2D minCoords,
                                                     Vector2D maxCoords) {
    Vector2D topLeftCoords = new Vector2D(minCoords.getX0(), maxCoords.getX1());
    Vector2D bottomLeftCoords = new Vector2D(maxCoords.getX0(), minCoords.getX1());
    return new JuliaCanvas(
        topLeftCoords,
        bottomLeftCoords
    );
  }

  public double getLastMousePosX() {
    return lastMousePosX;
  }

  public void setLastMousePosX(double lastMousePosX) {
    this.lastMousePosX = lastMousePosX;
  }

  public double getLastMousePosY() {
    return lastMousePosY;
  }

  public void setLastMousePosY(double lastMousePosY) {
    this.lastMousePosY = lastMousePosY;
  }

  public Vector2D getTopLeftCoords() {
    return topLeftCoords;
  }

  public void setTopLeftCoords(Vector2D topLeftCoords) {
    this.topLeftCoords = topLeftCoords;
  }

  public Vector2D getBottomRightCoords() {
    return bottomRightCoords;
  }

  public void setBottomRightCoords(Vector2D bottomRightCoords) {
    this.bottomRightCoords = bottomRightCoords;
  }

  /**
   * <h2>planeCoordsToPixelCoords</h2>
   * planeCoordsToPixelCoords.
   *
   * <p>
   * This method converts plane coordinates to pixel coordinates.
   * </p>
   *
   * @param vector The plane coordinates to convert.
   * @return The pixel coordinates.
   */
  public Vector2D planeCoordsToPixelCoords(Vector2D vector) {
    double width = this.getWidth();
    double height = this.getHeight();

    double x0 = getTopLeftCoords().getX0();
    double x1 = getBottomRightCoords().getX0();
    double y0 = getTopLeftCoords().getX1();
    double y1 = getBottomRightCoords().getX1();

    double x0RangeFactor = width / (x1 - x0);
    double x1RangeFactor = height / (y1 - y0);

    double x0Scaled = (vector.getX0() - x0) * x0RangeFactor;
    double x1Scaled = (vector.getX1() - y0) * x1RangeFactor;

    return new Vector2D(x0Scaled, x1Scaled);
  }

  /**
   * <h2>pixelCoordsToPlaneCoords</h2>
   * pixelCoordsToPlaneCoords.
   *
   * <p>
   * This method converts pixel coordinates to plane coordinates.
   * </p>
   *
   * @param pixel The pixel coordinates to convert.
   * @return The plane coordinates.
   */
  public Vector2D pixelCoordsToPlaneCoords(Vector2D pixel) {
    double width = this.getWidth();
    double height = this.getHeight();

    double x0 = getTopLeftCoords().getX0();
    double x1 = getBottomRightCoords().getX0();
    double y0 = getTopLeftCoords().getX1();
    double y1 = getBottomRightCoords().getX1();

    double x0RangeFactor = (x1 - x0) / width;
    double x1RangeFactor = (y1 - y0) / height;

    double coordinateX0 = pixel.getX0() * x0RangeFactor + x0;
    double coordinateX1 = pixel.getX1() * x1RangeFactor + y0;

    return new Vector2D(coordinateX0, coordinateX1);
  }

  protected abstract void onMouseDragged(MouseEvent event);

  protected abstract void onMousePressed(MouseEvent event);

  protected abstract void onMouseRelease(MouseEvent event);
}
