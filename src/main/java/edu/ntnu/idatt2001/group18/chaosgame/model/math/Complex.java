package edu.ntnu.idatt2001.group18.chaosgame.model.math;

/**
 * <h1>
 * Complex number class
 * </h1>
 * A class representing complex numbers.
 *
 * <p>
 * This class is used to represent complex numbers. It extends the Vector2D class as
 * complex numbers can easily be represented as vectors in 2D space,
 * where a (the real part) is x1 and b (the imaginary part) is x1.
 * </p>
 */
public class Complex extends Vector2D {
  public Complex(double a, double b) {
    super(a, b);
  }

  /**
   * <h2>
   * Constructor
   * </h2>
   * Constructor.
   *
   * <p>
   * Constructor for the Complex class.
   * </p>
   *
   * @param vector The vector to be converted to a complex number.
   */
  public Complex(Vector2D vector) {
    super(vector.getX0(), vector.getX1());
  }


  /**
   * <h3>
   * Square root of the complex number
   * </h3>
   * A method for calculating the square root of the complex number.
   *
   * <p>
   * This method is used to calculate and return an instance of the Complex class
   * representing the square root of the complex number the method was called upon.
   * </p>
   * <p>
   * See <a href="https://en.wikipedia.org/wiki/Complex_number#Square_root">...</a>
   * for more details about the actual calculation.
   * </p>
   *
   * @return A Complex object representing the square root of 'this' complex object.
   */
  public Complex[] sqrt() {
    // Array to hold both square roots
    Complex[] roots = new Complex[2];

    // Imaginary part must not be 0 to use the formula
    if (super.getX1() == 0) {
      // If the imaginary part is 0, we handle the real part separately
      if (getX0() < 0) {
        // For negative real parts, we return the square roots as pure imaginary numbers
        double imagPart = Math.sqrt(-getX0());
        roots[0] = new Complex(0, imagPart);
        roots[1] = new Complex(0, -imagPart);
      } else {
        // For non-negative real parts, the square roots are real numbers
        double realPart = Math.sqrt(getX0());
        roots[0] = new Complex(realPart, 0);
        roots[1] = new Complex(-realPart, 0);
      }
    } else {
      // See JavaDoc for details about calculation
      double r = Math.sqrt(Math.pow(getX0(), 2) + Math.pow(getX1(), 2));

      // Calculate the real part
      double a = Math.sqrt((getX0() + r) / 2);

      // Calculate the imaginary part
      double b = Math.sqrt((-getX0() + r) / 2);
      b *= Math.signum(getX1());

      // First root
      roots[0] = new Complex(a, b);

      // Second root is the conjugate of the first
      roots[1] = new Complex(-a, -b);
    }

    // Return the array containing both square roots
    return roots;
  }

  public Complex square() {
    return new Complex(
        Math.pow(getX0(), 2) - Math.pow(getX1(), 2),
        2 * getX0() * getX1()
    );
  }
}
