package edu.ntnu.idatt2001.group18.chaosgame.view.gui.sidebar.other;

import edu.ntnu.idatt2001.group18.chaosgame.utils.ObserverMessage;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.Observable;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.Observer;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

import java.util.ArrayList;

public class SaveImageView extends VBox implements Observable {
  private final ArrayList<Observer> observers = new ArrayList<>();
  private final Button saveImageButton = new Button("Save as Image");

  public SaveImageView() {
    super();

    this.getChildren().add(getSaveImageButton());

    getSaveImageButton().setOnAction(event -> notifyObservers(ObserverMessage.SAVE_IMAGE, null));
  }

  public Button getSaveImageButton() {
    saveImageButton.setPrefWidth(260);
    saveImageButton.setStyle("-fx-font-size: 16; -fx-background-color: " + "#9999ee" + "; -fx-text-fill: white; "
        + "-fx-font-weight: bold; -fx-effect: dropshadow(three-pass-box, rgba(0.2,0.2,0.2,0.3), 5, 0, 0, 2);");
    saveImageButton.setOnMouseEntered(e -> saveImageButton.setStyle("-fx-font-size: 16; -fx-cursor: hand; "
        + "-fx-scale-x: 1.02; -fx-scale-y: 1.02; -fx-background-color: " + "#aaaaff" + "; -fx-text-fill: white; "
        + "-fx-font-weight: bold; "
        + "-fx-effect: " + "dropshadow(three-pass-box, rgba(0.2,0.2,0.2,0.3), 5, 0, 0, 2);"));
    saveImageButton.setOnMouseExited(e -> saveImageButton.setStyle("-fx-font-size: 16; -fx-background-color: #9999ee;"
        + "-fx-text-fill: white; -fx-font-weight: bold; -fx-effect: dropshadow(three-pass-box, rgba(0.2,0.2,0.2,0.3), "
        + "5, 0, 0, 2);"));
    return saveImageButton;
  }

  @Override
  public ArrayList<Observer> getObservers() {
    return observers;
  }

  @Override
  public void addObserver(Observer observer) {
    getObservers().add(observer);
  }

  @Override
  public void removeObserver(Observer observer) {
    getObservers().remove(observer);
  }

  @Override
  public void notifyObservers(ObserverMessage message, Object[] data) {
    getObservers().forEach(o -> o.update(message, data));
  }
}
