package edu.ntnu.idatt2001.group18.chaosgame.model;

/**
 * <h2>Serializable</h2>
 * Serializable.
 *
 * <p>
 * This interface is used to represent a serializable object.
 * </p>
 */
public interface Serializable {
  /**
   * <h2>serialize</h2>
   * serialize.
   *
   * <p>
   * Method for serializing an object.
   * </p>
   *
   * @return The serialized object.
   */
  String serialize();
}
