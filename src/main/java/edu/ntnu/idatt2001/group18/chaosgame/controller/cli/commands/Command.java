package edu.ntnu.idatt2001.group18.chaosgame.controller.cli.commands;

import edu.ntnu.idatt2001.group18.chaosgame.view.cli.CliView;

/**
 * <h1>Command</h1>
 * Command.
 *
 * <p>
 * This abstract class represents a command that can be executed in the CLI.
 * </p>
 */
public abstract class Command {
  private final String command;
  private final String description;
  private final CliView cliView = new CliView();

  public Command(String name, String description) {
    this.command = name;
    this.description = description;
  }

  public String getCommand() {
    return command;
  }

  public String getDescription() {
    return description;
  }

  public CliView getCliView() {
    return cliView;
  }

  public abstract void execute();
}
