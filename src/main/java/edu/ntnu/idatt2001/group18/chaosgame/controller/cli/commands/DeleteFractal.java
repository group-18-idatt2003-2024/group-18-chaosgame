package edu.ntnu.idatt2001.group18.chaosgame.controller.cli.commands;

import edu.ntnu.idatt2001.group18.chaosgame.model.ChaosGameDescription;
import edu.ntnu.idatt2001.group18.chaosgame.utils.CliUtils;
import edu.ntnu.idatt2001.group18.chaosgame.utils.Constants;

import java.io.File;

public class DeleteFractal extends Command {
  public DeleteFractal(String command, String description) {
    super(command, description);
  }

  @Override
  public void execute() {
    ChaosGameDescription descriptionToDelete = CliUtils.pickOneFractal(
        "Enter the name of the one you want to delete."
    );

    if (descriptionToDelete == null) {
      return;
    }

    File fileToDelete = new File(Constants.FRACTAL_FOLDER_PATH + descriptionToDelete.getFileName());

    if (fileToDelete.delete()) {
      getCliView().print("Deleted '%s'%n%n", descriptionToDelete.getFileName());
    } else {
      getCliView().print("Failed to delete '%s'%n%n", descriptionToDelete.getFileName());
    }

    boolean deleteAnother = CliUtils.yesNo("Would you like to delete another one?");

    if (deleteAnother) {
      execute();
    }
  }
}
