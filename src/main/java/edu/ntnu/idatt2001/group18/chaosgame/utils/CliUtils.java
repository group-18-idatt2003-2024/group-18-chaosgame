package edu.ntnu.idatt2001.group18.chaosgame.utils;

import edu.ntnu.idatt2001.group18.chaosgame.model.ChaosGameDescription;
import java.util.Arrays;
import java.util.Optional;
import java.util.Scanner;

/**
 * <h1>CliUtils</h1>
 * CliUtils.
 *
 * <p>
 *   This class contains utility methods for the CLI.
 * </p>
 */
public class CliUtils {
  /**
   * <h2>PressEnterToContinue</h2>
   * PressEnterToContinue.
   *
   * <p>
   *   This method waits for the user to press enter before continuing.
   * </p>
   */
  public static void pressEnterToContinue() {
    System.out.print("\nPress enter to continue...");
    Scanner sc = new Scanner(System.in);
    sc.nextLine();
  }

  /**
   * <h2>PickOneFractal</h2>
   * PickOneFractal.
   *
   * <p>
   *   This method prompts the user to pick a fractal from a list of fractals.
   * </p>
   *
   * @param prompt  The prompt to show the user.
   * @return  The fractal the user picked.
   */
  public static ChaosGameDescription pickOneFractal(String prompt) {
    ChaosGameDescription[] fractals = ChaosGameFileHandler.getFractals();

    System.out.printf(
        "\nThere are currently %s fractal(s) stored. They are:\n\n",
        ChaosGameFileHandler.getFractalCount());

    Arrays.stream(fractals).forEach((fractal) -> System.out.println(fractal.getFileName()));

    Scanner sc = new Scanner(System.in);

    System.out.println("\nType 'exit' to go back to the main menu.");
    System.out.printf("%s\n> ", prompt);
    String response = sc.nextLine();

    if (response.equals("exit")) {
      return null; // null means exit
    }

    Optional<ChaosGameDescription> optionalFractal = Arrays
        .stream(fractals)
        .filter(fractal -> fractal.getFileName().toLowerCase().contains(response.toLowerCase()))
        .findFirst();

    if (optionalFractal.isPresent()) {
      return optionalFractal.get();
    } else {
      System.err.printf("No fractal found with the name '%s'%n", response);
      CliUtils.pressEnterToContinue();
      return pickOneFractal(prompt);
    }
  }

  /**
   * <h2>YesNo</h2>
   * YesNo.
   *
   * <p>
   *   This method prompts the user with a yes/no question.
   * </p>
   *
   * @param question  The question to ask the user.
   * @return  True if the user answered yes, false if the user answered no.
   */
  public static boolean yesNo(String question) {
    Scanner sc = new Scanner(System.in);

    System.out.printf("%s\n[Y/n] > ", question);
    String drawAnotherResponse = sc.nextLine();

    if (drawAnotherResponse.toLowerCase().contains("y") || drawAnotherResponse.isBlank()) {
      return true;
    } else if (!drawAnotherResponse.toLowerCase().contains("n")) {
      System.out.printf("\nCould not recognize option '%s'. Guessing it's a 'no'.\n",
          drawAnotherResponse);
      CliUtils.pressEnterToContinue();
    }

    return false;
  }
}
