package edu.ntnu.idatt2001.group18.chaosgame.controller.cli.commands;

import edu.ntnu.idatt2001.group18.chaosgame.model.ChaosGame;
import edu.ntnu.idatt2001.group18.chaosgame.model.ChaosGameDescription;
import edu.ntnu.idatt2001.group18.chaosgame.utils.CliUtils;

import java.util.Scanner;

public class DrawFractalCommand extends Command {
  private final Scanner sc = new Scanner(System.in);

  public DrawFractalCommand(String command, String description) {
    super(command, description);
  }

  @Override
  public void execute() {
    ChaosGameDescription description = CliUtils.pickOneFractal("Which one would you like to draw?");

    if (description == null) {
      return;
    }

    continueWithDescription(description);

    boolean goAgain = CliUtils.yesNo("Draw another one?");

    if (goAgain) {
      execute();
    }
  }

  private void continueWithDescription(ChaosGameDescription fractal) {
    try {
      getCliView().print("Screensize:");
      int screenSize = Integer.parseInt(getCliView().getInput("[x] > "));

      getCliView().print("Steps:");
      // using sc.nextInt() causes next sc.nextLine() to be skipped on next use
      int stepsToRun = Integer.parseInt(getCliView().getInput("[x] > "));

      ChaosGame game = new ChaosGame(fractal, screenSize);
      game.run(stepsToRun);
      game.getCliCanvas().printCanvas();

    } catch (IllegalArgumentException e) {
      System.err.printf("An error has occurred: %s%n", e.getMessage());
      CliUtils.pressEnterToContinue();
    }
  }
}
