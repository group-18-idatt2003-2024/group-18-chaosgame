package edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation;

import edu.ntnu.idatt2001.group18.chaosgame.model.math.Matrix2x2;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.Vector2D;
import java.util.Arrays;

/**
 * <h2>
 * AffineTransform2D class
 * </h2>
 * Class for affine transformations.
 *
 * <p>
 * This class is used to represent an affine transformation in 2D space.
 * It extends the Transform2D class and implements the transform method.
 * </p>
 */
public class AffineTransform2D extends Transform2D {
  private final Matrix2x2 matrix;
  private final Vector2D vector;

  /**
   * <h2>
   * Constructor
   * </h2>
   * Constructor.
   *
   * <p>
   * Constructor for the AffineTransform2D-class.
   * </p>
   *
   * @param matrix The 2x2 matrix used by the transformation.
   * @param vector The 2D shifting vector used by the transformation.
   */
  public AffineTransform2D(Matrix2x2 matrix, Vector2D vector) {
    super();
    this.matrix = matrix;
    this.vector = vector;
  }

  /**
   * <h1>
   * fromSerial
   * </h1>
   * fromSerial.
   *
   * <p>
   * This method returns an instance of the AffineTransform2D-class from
   * a given serial.
   * </p>
   *
   * @param serialString String The raw string-serial to create an instance from.
   * @return The instance created from a serial.
   */
  public static AffineTransform2D fromSerial(String serialString) {
    double[] serial = Arrays
        .stream(serialString.split(","))
        .mapToDouble(Double::parseDouble)
        .toArray();

    Matrix2x2 matrix = new Matrix2x2(
        serial[0],
        serial[1],
        serial[2],
        serial[3]
    );

    Vector2D vector = new Vector2D(
        serial[4],
        serial[5]
    );

    return new AffineTransform2D(matrix, vector);
  }

  public Matrix2x2 getMatrix() {
    return matrix;
  }

  public Vector2D getVector() {
    return vector;
  }

  /**
   * <h3>
   * AffineTransform2D.transform()
   * </h3>
   *
   * <p>
   * This method calculates the affine transformation of the given vector.
   * Formula for transformation: Ax + b
   * </p>
   *
   * @param point The vector to be transformed.
   * @return The transformed vector.
   */
  @SuppressWarnings("checkstyle:LocalVariableName")
  public Vector2D transform(Vector2D point) {
    // Calculate Ax
    Vector2D Ax = matrix.multiply(point);

    // Calculate Ax + b
    return new Vector2D(
        Ax.getX0() + vector.getX0(),
        Ax.getX1() + vector.getX1());
  }

  @Override
  public String serialize() {
    return matrix.serialize() + "," + vector.serialize();
  }
}
