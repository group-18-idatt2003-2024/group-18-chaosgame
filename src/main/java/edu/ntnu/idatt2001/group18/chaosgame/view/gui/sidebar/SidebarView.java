package edu.ntnu.idatt2001.group18.chaosgame.view.gui.sidebar;

import edu.ntnu.idatt2001.group18.chaosgame.view.gui.sidebar.other.HelpView;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.sidebar.other.SaveImageView;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.sidebar.settings.CreateCanvasView;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.sidebar.settings.FilesView;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.sidebar.settings.StepsView;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

public class SidebarView extends BorderPane {
  private final StepsView stepsView = new StepsView();
  private final CreateCanvasView createCanvasView = new CreateCanvasView();

  private final FilesView filesView = new FilesView();
  private final SaveImageView saveImageView = new SaveImageView();
  private final HelpView helpView = new HelpView();

  /**
   * <h2>Constructor</h2>
   * Constructor.
   *
   * <p>
   * Constructor for the SidebarView class. This class acts as the sidebar in the GUI
   * where all the settings are located.
   * </p>
   */
  public SidebarView() {
    setPadding(new Insets(20));
    maxWidthProperty().bind(widthProperty());

    VBox topBox = new VBox();
    topBox.getChildren().addAll(
        chaosGameTitle(),
        getCreateCanvasView(),
        getFilesView(),
        getStepsView()
    );
    setTop(topBox);

    VBox botBox = new VBox();
    botBox.setSpacing(10);
    botBox.getChildren().addAll(
        getSaveImageView(),
        getHelpView(),
        exitButton()
    );

    setBottom(botBox);
  }

  public Label chaosGameTitle() {
    Label chaosGameTitle = new Label("Chaos Game");
    chaosGameTitle.setAlignment(javafx.geometry.Pos.CENTER);
    chaosGameTitle.prefWidthProperty().bind(this.widthProperty());
    chaosGameTitle.setPadding(new Insets(10, 0, 30, 0));
    chaosGameTitle.setStyle("-fx-font-size: 32; -fx-font-weight: bold");
    return chaosGameTitle;
  }

  public StepsView getStepsView() {
    return stepsView;
  }

  public CreateCanvasView getCreateCanvasView() {
    return createCanvasView;
  }

  public FilesView getFilesView() {
    return filesView;
  }

  public SaveImageView getSaveImageView() {
    return saveImageView;
  }

  public HelpView getHelpView() {
    return helpView;
  }

  private Button exitButton() {
    setStyle("-fx-background-color: #eeeeee");

    Button exitButton = styledButton();

    exitButton.setPrefHeight(40);
    exitButton.setPrefWidth(260);
    exitButton.setOnAction(e -> System.exit(0));
    return exitButton;
  }

  private Button styledButton() {
    Button button = new Button("Exit");
    button.setStyle("-fx-font-size: 16; -fx-background-color: " + "#bb2222" + "; -fx-text-fill: white; -fx-font-weight: " +
        "bold; -fx-effect: dropshadow(three-pass-box, rgba(0.2,0.2,0.2,0.3), 5, 0, 0, 2);");
    button.setOnMouseEntered(e -> button.setStyle("-fx-font-size: 16; -fx-cursor: hand; -fx-scale-x: 1.02; " +
        "-fx-scale-y: 1.02; -fx-background-color: " + "#cd3333" + "; -fx-text-fill: white; -fx-font-weight: bold; "
        + "-fx-effect: " + "dropshadow(three-pass-box, rgba(0.2,0.2,0.2,0.3), 5, 0, 0, 2);"));
    button.setOnMouseExited(e -> button.setStyle("-fx-font-size: 16; -fx-background-color: " + "#bb2222" + "; -fx-text" +
        "-fill: white; -fx-font-weight: bold; -fx-effect: dropshadow(three-pass-box, rgba(0.2,0.2,0.2,0.3), "
        + "5, 0, 0, 2);"));
    return button;
  }
}
