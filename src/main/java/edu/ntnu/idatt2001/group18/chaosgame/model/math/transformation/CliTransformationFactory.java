package edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation;

import edu.ntnu.idatt2001.group18.chaosgame.model.math.Complex;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.Matrix2x2;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.Vector2D;
import edu.ntnu.idatt2001.group18.chaosgame.utils.CliUtils;
import edu.ntnu.idatt2001.group18.chaosgame.view.cli.CliView;
import java.util.Arrays;

/**
 * <h1>CliTransformationFactory</h1>
 * CliTransformationFactory.
 *
 * <p>
 *   This class is a factory for creating transformations from the CLI.
 * </p>
 */
public class CliTransformationFactory {
  /**
   * <h2>CreateTransformation</h2>
   * CreateTransformation.
   *
   * <p>
   *   Create a transformation from the CLI.
   *   The user is prompted to enter the values for the transformation.
   * </p>
   *
   * @param transformType The type of transformation to create.
   * @param transformationNumber  The number of the transformation.
   * @return  The created transformation.
   */
  public static Transform2D createTransformation(
      Class<? extends Transform2D> transformType,
      int transformationNumber) {
    if (transformType == AffineTransform2D.class) {
      return createAffineTransformation(transformationNumber);
    } else if (transformType == JuliaTransform.class) {
      return createJuliaTransform();
    } else {
      throw new IllegalArgumentException(
          String.format(
              "Transformation '%s' does not exist%n",
              transformType.getSimpleName()
          )
      );
    }
  }

  private static AffineTransform2D createAffineTransformation(Integer transformationNumber) {
    CliView cliView = new CliView();

    try {
      cliView.print("Transformation %s%n", String.valueOf(transformationNumber));
      String matrixValueString = cliView.getInput(
          "Enter the values for the scaling matrix: [a,b,c,d]\n"
      );
      double[] matrixValues = Arrays
          .stream(matrixValueString.split(","))
          .mapToDouble(Double::parseDouble)
          .toArray();

      String shiftvectorString = cliView.getInput(
          "Enter the values for the shifting vector: [x,y]\n"
      );
      double[] vectorValues = Arrays
          .stream(shiftvectorString
              .split(","))
          .mapToDouble(Double::parseDouble)
          .toArray();

      Matrix2x2 matrix = new Matrix2x2(
          matrixValues[0],
          matrixValues[1],
          matrixValues[2],
          matrixValues[3]
      );
      Vector2D vector = new Vector2D(vectorValues[0], vectorValues[1]);

      return new AffineTransform2D(matrix, vector);
    } catch (Exception e) {
      System.err.println("You did not enter valid data.");
      if (CliUtils.yesNo("Try again?")) {
        return createAffineTransformation(transformationNumber);
      }
      return null;
    }
  }

  private static JuliaTransform createJuliaTransform() {
    CliView cliView = new CliView();

    try {
      String complexValuesString = cliView.getInput(
          "Enter the values for the complex number\n[a,b] > ");
      double[] complexValues = Arrays
          .stream(complexValuesString
              .split(","))
          .mapToDouble(Double::parseDouble)
          .toArray();

      cliView.print("Enter the sign (must be 1 or -1)");
      int sign = Integer.parseInt(cliView.getInput("[x] > "));

      if (sign != 1 && sign != -1) {
        throw new IllegalArgumentException("Sign must be 1 or -1");
      }

      Complex complex = new Complex(complexValues[0], complexValues[1]);

      return new JuliaTransform(complex, sign);
    } catch (Exception e) {
      System.err.println("You entered invalid data.");
      if (CliUtils.yesNo("Try again?")) {
        return createJuliaTransform();
      }
      return null;
    }
  }
}
