package edu.ntnu.idatt2001.group18.chaosgame.model;

import edu.ntnu.idatt2001.group18.chaosgame.controller.cli.ChaosCliCanvas;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.Vector2D;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation.AffineTransform2D;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation.JuliaTransform;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation.Transform2D;
import edu.ntnu.idatt2001.group18.chaosgame.utils.Constants;
import edu.ntnu.idatt2001.group18.chaosgame.utils.ObserverMessage;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.Observable;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.Observer;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.center.ChaosGameCanvasView;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.center.canvas.JuliaCanvas;
import java.util.ArrayList;
import java.util.Random;

/**
 * <h1>ChaosGame</h1>
 * ChaosGame.
 *
 * <p>
 * This class represents a 'game' that can be run. It includes a set of transformations as well
 * as a cli canvas.
 * </p>
 */
public class ChaosGame implements Observable {
  private final ArrayList<Observer> observers = new ArrayList<>();
  private final Random random = new Random();
  private final ChaosGameDescription description;
  private ChaosCliCanvas cliCanvas = null;
  private Vector2D currentPoint = new Vector2D(0, 0);

  /**
   * <h2>
   * Constructor
   * </h2>
   * Constructor.
   *
   * <p>
   * Constructor for the class.
   * </p>
   *
   * @param description The description of the chaos game.
   * @param size        The size of the canvas.
   */
  public ChaosGame(ChaosGameDescription description, int size) {
    this.cliCanvas = new ChaosCliCanvas(size * 2, size, description.getMinCoords(),
        description.getMaxCoords());
    this.description = description;
  }

  public ChaosGame(ChaosGameDescription description) {
    this.description = description;
  }

  public ChaosCliCanvas getCliCanvas() {
    return cliCanvas;
  }

  public ChaosGameDescription getDescription() {
    return description;
  }

  public Vector2D getCurrentPoint() {
    return currentPoint;
  }

  public void setCurrentPoint(Vector2D currentPoint) {
    this.currentPoint = currentPoint;
  }


  /**
   * <h2>runSteps</h2>
   * runSteps.
   *
   * <p>
   * This method will run the game (execute transformations) a given amount of times. The technique
   * used depends on what type of transformations we are working with.
   * </p>
   *
   * @param steps The amount of transformations to do.
   */
  public void run(int steps) {
    // If the transformations are affine transformations
    if (getDescription().getTransforms().getFirst() instanceof AffineTransform2D) {
      runAffine(steps);
    } else if (getDescription().getTransforms().getFirst() instanceof JuliaTransform) {
      runJulia(steps);
    } else {
      throw new IllegalStateException(
          String.format(
              "Unknown transformation type: %s%n",
              getDescription().getTransforms().getFirst().getClass().getSimpleName()
          )
      );
    }

    System.out.println("DONE");
  }

  private void runJulia(int steps) {
    final double escapeRadiusSquared = 4;
    JuliaTransform transformation = (JuliaTransform) description.getTransforms().getFirst();
    if (cliCanvas == null) {

      observers.forEach(o -> {
        ChaosGameCanvasView observer = (ChaosGameCanvasView) o;
        final JuliaCanvas canvas = (JuliaCanvas) observer.getCanvas();
        final int width = (int) canvas.getWidth();
        final int height = (int) canvas.getHeight();
        int iterations;
        boolean hasNotEscaped;
        double zx;
        double zy;

        for (int y = 0; y < height; y++) {
          for (int x = 0; x < width; x++) {
            Vector2D currentPoint = canvas.pixelCoordsToPlaneCoords(new Vector2D(x, y));

            iterations = 0;
            hasNotEscaped = true;

            while (hasNotEscaped) {
              currentPoint = transformation.transform(currentPoint);
              zx = currentPoint.getX0();
              zy = currentPoint.getX1();
              hasNotEscaped = zx * zx + zy * zy < escapeRadiusSquared
                  && iterations < Constants.JULIA_MAX_ITERATIONS;

              iterations++;
            }

            observer.update(
                ObserverMessage.NEW_POSITION,
                new Object[]{new Vector2D(x, y),
                    iterations}
            );
          }
        }
      });
    } else {
      for (int i = 0; i < steps; i++) {
        // Transform the current point
        setCurrentPoint(transformation.cliTransform(currentPoint));

        // Draw the new point
        cliCanvas.putPixel(new Vector2D(getCurrentPoint().getX0(), getCurrentPoint().getX1()));
      }
    }
  }

  private void runAffine(int steps) {
    for (int i = 0; i < steps; i++) {
      // Get a random transformation
      int transformIndex = random.nextInt(description.getTransforms().size());
      Transform2D transformation = description.getTransforms().get(transformIndex);

      // Transform the current point
      setCurrentPoint(transformation.transform(getCurrentPoint()));

      // Draw the new point
      if (cliCanvas == null) {
        // Notify the observers
        observers.forEach(o -> {
          ChaosGameCanvasView observer = (ChaosGameCanvasView) o;
          Vector2D pixelCoords = observer.getCanvas().planeCoordsToPixelCoords(getCurrentPoint());
          observer.update(ObserverMessage.NEW_POSITION, new Object[]{pixelCoords, 0});
        });
      } else {
        cliCanvas.putPixel(getCurrentPoint());
      }
    }
  }

  @Override
  public ArrayList<Observer> getObservers() {
    return observers;
  }

  @Override
  public void addObserver(Observer observer) {
    if (!getObservers().contains(observer)) {
      getObservers().add(observer);
    }
  }

  @Override
  public void removeObserver(Observer observer) {
    getObservers().remove(observer);
  }

  @Override
  public void notifyObservers(ObserverMessage message, Object[] data) {
    getObservers().forEach(o -> o.update(message, data));
  }
}
