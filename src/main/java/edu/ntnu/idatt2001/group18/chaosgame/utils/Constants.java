package edu.ntnu.idatt2001.group18.chaosgame.utils;

import javafx.scene.control.Label;

public class Constants {
  public static final String FRACTAL_FOLDER_PATH = "fractals/";

  public static final String BANNER = """
       
       
       ██████╗██╗  ██╗ █████╗  ██████╗ ███████╗     ██████╗  █████╗ ███╗   ███╗███████╗
      ██╔════╝██║  ██║██╔══██╗██╔═══██╗██╔════╝    ██╔════╝ ██╔══██╗████╗ ████║██╔════╝
      ██║     ███████║███████║██║   ██║███████╗    ██║  ███╗███████║██╔████╔██║█████╗ \s
      ██║     ██╔══██║██╔══██║██║   ██║╚════██║    ██║   ██║██╔══██║██║╚██╔╝██║██╔══╝ \s
      ╚██████╗██║  ██║██║  ██║╚██████╔╝███████║    ╚██████╔╝██║  ██║██║ ╚═╝ ██║███████╗
       ╚═════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚══════╝     ╚═════╝ ╚═╝  ╚═╝╚═╝     ╚═╝╚══════╝
                                                                                      \s
            \s""";


  public static final String GOODBYE = """
       
       
       ██████╗  ██████╗  ██████╗ ██████╗ ██████╗ ██╗   ██╗███████╗██╗
      ██╔════╝ ██╔═══██╗██╔═══██╗██╔══██╗██╔══██╗╚██╗ ██╔╝██╔════╝██║
      ██║  ███╗██║   ██║██║   ██║██║  ██║██████╔╝ ╚████╔╝ █████╗  ██║
      ██║   ██║██║   ██║██║   ██║██║  ██║██╔══██╗  ╚██╔╝  ██╔══╝  ╚═╝
      ╚██████╔╝╚██████╔╝╚██████╔╝██████╔╝██████╔╝   ██║   ███████╗██╗
       ╚═════╝  ╚═════╝  ╚═════╝ ╚═════╝ ╚═════╝    ╚═╝   ╚══════╝╚═╝
            \n""";

  public static final String CONSOLE_PIXEL = "█";
  public static final int JULIA_MAX_ITERATIONS = 1000;
  public static final double ZOOM_FACTOR = 1.16;

  public static final int JULIA_CANVAS_SIZE = 1000;
  public static final Label HELP_LABEL = new Label("""
      Hello, and welcome to ChaosGame!
            
      This application can visualize fractals based on iterative transformations.
            
      This help screen is always available by clicking the 'HELP' button!
            
      Before you start, make sure you have a folder called 'fractals' in the same
      directory as the application. This folder will contain the fractals you create
      and edit.
            
      Selecting, editing and creating a fractal:
      1. You can select one of the fractals in the sidebar by clicking it.
      2. You can also edit its parameters by double-clicking or by clicking the
      'EDIT' button.
      3. When editing a fractal you can hit one the toggle buttons to switch between
      affine and julia fractals.
      4. For affine fractals you can add and delete transformations by clicking the
      'ADD' and 'DELETE' buttons.
      When you're done editing you can save the fractal by clicking the 'SAVE' button.
      5. You can also create a new fractal by clicking the 'NEW' button, or delete
      the selected fractal by clicking the 'DELETE' button.
            
      Running a fractal:
      1. Make sure you have selected a fractal.
      2. If its affine, specify an amount of steps to do.
      3. Hit the 'RUN' button.
            
      Change canvas size (only for affine, not needed for julia):
      1. Make sure you have selected an affine fractal and ran it atleast once after
          selecting it.
      2. Set a canvas size
      3. Hit the 'CREATE CANVAS' button.
            
      Zooming affine fractals:
      Use the scrollwheel on the canvas to zoom in and out, centered around the
      mouse cursor, and hold left click and pan around to move the canvas.
            
      Zooming julia fractals:
      Draw a square on the canvas by holding left click and dragging the mouse
      diagonally. Right click to zoom out to the previous zoom.
      NOTE: JULIA FRACTALS ARE COMPUTATIONALLY INTENSIVE AND MAY TAKE A WHILE TO
      COMPLETE (EVEN WHEN ZOOMING OUT).
            
      Have fun! :)\s
      """);
}
