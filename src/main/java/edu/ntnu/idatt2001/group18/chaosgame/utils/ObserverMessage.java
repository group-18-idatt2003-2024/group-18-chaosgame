package edu.ntnu.idatt2001.group18.chaosgame.utils;

public enum ObserverMessage {
  NEW_POSITION,
  RUN_GAME,
  NEW_ZOOM,
  SAVE_FILE,
  LOAD_FILE,
  SELECT_GAME,
  NEW_CANVAS,
  CHANGE_EDITOR,
  SAVE_IMAGE,
  LOADING
}
