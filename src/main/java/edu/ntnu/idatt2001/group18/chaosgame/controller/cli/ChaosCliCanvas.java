package edu.ntnu.idatt2001.group18.chaosgame.controller.cli;

import edu.ntnu.idatt2001.group18.chaosgame.model.math.Matrix2x2;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.Vector2D;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation.AffineTransform2D;
import edu.ntnu.idatt2001.group18.chaosgame.utils.Constants;

/**
 * <h1>ChaosCanvas</h1>
 * ChaosCanvas.
 *
 * <p>
 * Class for representing a canvas for the chaos game. The canvas is
 * represented as a 2D array of integers, where each integer represents a pixel.
 * The canvas also has a transformation from coordinates to indices in the 2D
 * array, so that the user can draw on the canvas using coordinates.
 * </p>
 */
public class ChaosCliCanvas {
  private final AffineTransform2D transformCoordsToIndices;
  private final int[][] canvas;

  /**
   * <h2>Constructor</h2>
   * Constructor.
   *
   * @param width     The width of the canvas.
   * @param height    The height of the canvas.
   * @param minCoords The minimum coordinate of the fractal.
   * @param maxCoords The maximum coordinate of the fractal.
   */
  public ChaosCliCanvas(int width, int height, Vector2D minCoords, Vector2D maxCoords) {
    super();
    this.canvas = new int[height][width];

    // Calculate the transformation from coordinates to indices.
    double deltaCoordsX = maxCoords.getX0() - minCoords.getX0();
    double deltaCoordsY = maxCoords.getX1() - minCoords.getX1();
    double deltaIndicesX = width - 1;
    double deltaIndicesY = height - 1;

    Matrix2x2 scaleMatrix = new Matrix2x2(
        deltaIndicesX / deltaCoordsX,
        0,
        0,
        deltaIndicesY / deltaCoordsY * -1 // Negative because the y-axis is flipped.
    );

    Vector2D offsetVector = new Vector2D(
        minCoords.getX0() * deltaIndicesX / deltaCoordsX * -1,
        maxCoords.getX1() * deltaIndicesY / deltaCoordsY);

    this.transformCoordsToIndices = new AffineTransform2D(scaleMatrix, offsetVector);
  }

  public int[][] getCanvasArray() {
    return canvas;
  }

  /**
   * <h2>putPixel</h2>
   * putPixel.
   *
   * <p>
   * Put a pixel at a given point.
   * </p>
   *
   * @param point The fractal point to put the pixel at.
   */
  public void putPixel(Vector2D point) {
    Vector2D pixelCoordinate = transformCoordsToIndices.transform(point);

    boolean isInsideMinY = pixelCoordinate.getX1() >= 0;
    boolean isInsideMaxY = pixelCoordinate.getX1() <= getCanvasArray().length - 1;
    boolean isInsideMinX = pixelCoordinate.getX0() >= 0;
    boolean isInsideMaxX = pixelCoordinate.getX0() <= getCanvasArray()[0].length - 1;

    boolean isInsideCanvas = isInsideMinY && isInsideMaxY && isInsideMinX && isInsideMaxX;

    if (isInsideCanvas) {
      getCanvasArray()
          [getCanvasArray().length - (int) pixelCoordinate.getX1()]
          [(int) pixelCoordinate.getX0()] = 1;
    }
  }

  /**
   * <h2>printCanvas</h2>
   * printCanvas.
   *
   * <p>
   * Print the canvas to the console.
   * </p>
   */
  public void printCanvas() {
    System.out.println("_".repeat(getCanvasArray()[0].length + 1));
    for (int y = getCanvasArray().length - 1; y > 0; y--) {
      System.out.print("|");
      for (int x = 0; x < getCanvasArray()[0].length; x++) {
        boolean isLit = getCanvasArray()[y][x] == 1;
        System.out.print(isLit ? Constants.CONSOLE_PIXEL : " ");
      }
      System.out.println("|");
    }
    System.out.println("¯".repeat(getCanvasArray()[0].length + 1));
  }
}
