package edu.ntnu.idatt2001.group18.chaosgame.controller.cli.commands;

import edu.ntnu.idatt2001.group18.chaosgame.model.ChaosGameDescription;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation.AffineTransform2D;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation.CliTransformationFactory;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation.JuliaTransform;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation.Transform2D;
import edu.ntnu.idatt2001.group18.chaosgame.utils.ChaosGameFileHandler;
import edu.ntnu.idatt2001.group18.chaosgame.utils.CliUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class CreateNewFractal extends Command {
  public CreateNewFractal(String command, String description) {
    super(command, description);
  }

  @Override
  public void execute() {
    try {
      Scanner sc = new Scanner(System.in);

      ArrayList<Transform2D> transforms = new ArrayList<>();

      HashMap<String, Class<? extends Transform2D>> transformHash = new HashMap<>() {
        {
          put("1", AffineTransform2D.class);
          put("2", JuliaTransform.class);
        }
      };

      getCliView().print("Transformation - Which transformation type do you want? Leave blank to exit. These are your options:\n");
      getCliView().print("1 %s\n2 %s", AffineTransform2D.class.getSimpleName(), JuliaTransform.class.getSimpleName());

      String chosenTransformString = getCliView().getInput("\n[x] > ");

      if (chosenTransformString.isBlank()) {
        return;
      } else if (!transformHash.containsKey(chosenTransformString)) {
        getCliView().print("Invalid input. Please try again.");
        execute();
      }

      Class<? extends Transform2D> chosenTransform = transformHash.get(chosenTransformString);

      int numberOfTransformations;
      if (chosenTransform.equals(AffineTransform2D.class)) {
        getCliView().print("How many transformations do you want?\n");
        numberOfTransformations = Integer.parseInt(getCliView().getInput("[x] > "));
      } else {
        numberOfTransformations = 1;
      }

      for (int i = 0; i < numberOfTransformations; i++) {
        Transform2D transform = CliTransformationFactory.createTransformation(chosenTransform, i + 1);

        if (transform == null) {
          return;
        }

        transforms.add(transform);
      }

      getCliView().print(
          "Please enter the smallest x-value, largest x-value and the smallest y-value of the fractal,"
              + "\nthe largest y-value is calculated automatically:\n"
      );

      String minMaxCoordsString = getCliView().getInput("[x1,x2,y1] > ");
      double[] minMaxValues = Arrays
          .stream(minMaxCoordsString.split(","))
          .mapToDouble(Double::parseDouble)
          .toArray();

      getCliView().print("Give the fractal a name:\n");
      String name = getCliView().getInput("[abc] > ");

      ChaosGameDescription description = new ChaosGameDescription(
          name,
          transforms,
          minMaxValues[0],
          minMaxValues[1],
          minMaxValues[2]
      );

      ChaosGameFileHandler.writeToFile(description);
    } catch (IllegalArgumentException e) {
      System.err.println(e.getMessage());

      if (CliUtils.yesNo("Try again?")) {
        execute();
      }
    }
  }
}
