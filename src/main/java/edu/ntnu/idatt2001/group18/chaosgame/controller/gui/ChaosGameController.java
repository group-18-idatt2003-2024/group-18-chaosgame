package edu.ntnu.idatt2001.group18.chaosgame.controller.gui;

import edu.ntnu.idatt2001.group18.chaosgame.model.ChaosGame;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation.JuliaTransform;
import edu.ntnu.idatt2001.group18.chaosgame.utils.ChaosGameFileHandler;
import edu.ntnu.idatt2001.group18.chaosgame.utils.ObserverMessage;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.Observer;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.center.ChaosGameCanvasView;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.sidebar.SidebarView;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;

/**
 * <h2>ChaosGameController</h2>
 * ChaosGameController.
 *
 * <p>
 * This class is the controller for the chaos game. It is responsible for handling the logic
 * of the chaos game and updating the view accordingly.
 * It is also responsible for handling the user input and updating the model accordingly.
 * </p>
 */
public class ChaosGameController extends BorderPane implements Observer {
  private final SidebarView sideBarView = new SidebarView();
  private final ChaosGameCanvasView canvasView = new ChaosGameCanvasView();
  private ChaosGame game = null;
  private File selectedFile;
  private File renderedFile;

  /**
   * <h2>Constructor</h2>
   * Constructor.
   *
   * <p>
   * Constructor for the ChaosGameController class. It heavily utilized the observer pattern
   * to handle communications between the view and the model.
   * </p>
   */
  public ChaosGameController() {
    super();

    // Add observers to the observables
    getSideBarView().getFilesView().addObserver(this);
    getSideBarView().getStepsView().addObserver(this);
    getSideBarView().getCreateCanvasView().addObserver(getCanvasView());
    getSideBarView().getSaveImageView().addObserver(this);
    getCanvasView().addObserver(this);

    // Set the contents of the BorderPane
    setLeft(getSideBarView());
    setCenter(canvasView);
  }

  public SidebarView getSideBarView() {
    return sideBarView;
  }

  private ChaosGameCanvasView getCanvasView() {
    return canvasView;
  }

  private ChaosGame getGame() {
    return game;
  }

  private void setGame(ChaosGame game) {
    this.game = game;
  }


  @Override
  public void update(ObserverMessage message, Object[] data) {
    switch (message) {
      case ObserverMessage.SELECT_GAME:
        selectedFile = (File) data[0];
        getSideBarView().getStepsView().toggleStepVisibility(
            ChaosGameFileHandler.getTransformationType(selectedFile).equals(
            "AffineTransform2D"));
        getSideBarView().getCreateCanvasView().toggleCanvasSizeVisibility(
            ChaosGameFileHandler.getTransformationType(selectedFile).equals(
            "AffineTransform2D"));

        getSideBarView().getFilesView().toggleEditorPaneVisibility(false);
        getSideBarView().getFilesView().initializeEditors();
        break;

      case ObserverMessage.RUN_GAME:
        int steps = (int) data[0];
        if (selectedFile == null) {
          System.out.println("No game loaded. Please select a file first.");
        } else {
          runGameFromFile(selectedFile, steps);
        }
        break;

      case ObserverMessage.NEW_ZOOM:
        if (getGame().getDescription().getTransforms().getFirst() instanceof JuliaTransform) {
          getGame().run(1);
        } else {
          throw new IllegalStateException("Can only perform true canvas zoom on julia transforms");
        }
        break;

      case ObserverMessage.NEW_CANVAS:
        // data = {new size}
        int size = (int) Math.floor((double) data[0]);
        getSideBarView().getCreateCanvasView().getSizeField().setText(String.valueOf(size));
        break;

      case ObserverMessage.SAVE_FILE:
        String content = (String) data[0];
        File saveFile = (File) data[1];
        try {
          Files.writeString(saveFile.toPath(), content);
          game = null;
        } catch (IOException e) {
          System.err.printf(
              "Error: %s. Failed to read directory: %s%n",
              e.getMessage(),
              saveFile.toPath()
          );
        }
        break;

      case ObserverMessage.LOAD_FILE:
        File loadFile = (File) data[0];
        try {
          String fileContent = Files.readString(loadFile.toPath());
          getSideBarView().getFilesView().updateEditor(fileContent, loadFile);
          // Possibly set the loaded file in the game or update the model
        } catch (IOException e) {
          getSideBarView().getFilesView().handleFileLoadError(e.getMessage());
        }
        break;

      case ObserverMessage.SAVE_IMAGE:
        if (getCanvasView().getCanvas().getHeight() != 0) {
          FileChooser fileChooser = new FileChooser();
          fileChooser.setTitle("Save fractal as image");
          FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
              "PNG files",
              "*.PNG"
          );
          fileChooser.getExtensionFilters().add(extFilter);
          File file = fileChooser.showSaveDialog(new Stage());
          if (file != null) {
            try {
              final int canvasSize = (int) getCanvasView().getCanvas().getWidth();
              WritableImage writableImage = new WritableImage(canvasSize, canvasSize);
              getCanvasView().getCanvas().snapshot(null, writableImage);
              RenderedImage renderedImage = SwingFXUtils.fromFXImage(writableImage, null);
              ImageIO.write(renderedImage, "png", file);
            } catch (IOException e) {
              e.printStackTrace();
            }
          }
        }
        break;

      default:
        System.err.printf("Observermessage %s not recognized.%n", message);
    }
  }

  private void runGameFromFile(File file, int steps) {
    if (game == null || !file.equals(renderedFile)) {
      // Load a new game only if necessary
      setGame(new ChaosGame(ChaosGameFileHandler.readFromFile(file)));
      getGame().addObserver(getCanvasView());
      getCanvasView().createCanvasFromDescription(getGame().getDescription());
      renderedFile = file;
    }
    System.out.printf("Running game from file: %s for %d steps%n", file.getName(), steps);
    game.run(steps);
  }
}
