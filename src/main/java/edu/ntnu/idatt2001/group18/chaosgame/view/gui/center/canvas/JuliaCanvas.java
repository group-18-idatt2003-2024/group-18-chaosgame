package edu.ntnu.idatt2001.group18.chaosgame.view.gui.center.canvas;

import edu.ntnu.idatt2001.group18.chaosgame.model.math.Vector2D;
import edu.ntnu.idatt2001.group18.chaosgame.utils.ObserverMessage;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.Observable;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.Observer;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.paint.Color;

import java.util.ArrayList;

import static edu.ntnu.idatt2001.group18.chaosgame.utils.Constants.JULIA_CANVAS_SIZE;
import static edu.ntnu.idatt2001.group18.chaosgame.utils.Constants.ZOOM_FACTOR;

/**
 * <h2>JuliaCanvas</h2>
 * JuliaCanvas.
 *
 * <p>
 * This class is a canvas for the Julia set. It is purely responsible for displaying the Julia set.
 * </p>
 */
public class JuliaCanvas extends ChaosGameCanvas implements Observable {
  private final ArrayList<Observer> observers = new ArrayList<>();
  private final ArrayList<Vector2D[]> zoomLog = new ArrayList<>();
  private final Canvas canvasCopy = new Canvas();

  /**
   * <h2>Constructor</h2>
   * Constructor.
   *
   * <p>
   * Constructor for the JuliaCanvas class.
   * </p>
   *
   * @param topLeftCoords     The top left plane coordinates of the canvas.
   * @param bottomRightCoords The bottom right plane coordinates of the canvas.
   */
  public JuliaCanvas(Vector2D topLeftCoords, Vector2D bottomRightCoords) {
    super(JULIA_CANVAS_SIZE, JULIA_CANVAS_SIZE, topLeftCoords, bottomRightCoords);

    canvasCopy.setWidth(JULIA_CANVAS_SIZE);
    canvasCopy.setHeight(JULIA_CANVAS_SIZE);

    canvasCopy.setScaleX(1.25);
    canvasCopy.setScaleY(1.25);

    setScaleX(0.8);
    setScaleY(0.8);

    // Add the initial zoom to the zoom log
    zoomLog.add(new Vector2D[]{topLeftCoords, bottomRightCoords});

    // Change the zoom on scrolling
    this.setOnScroll((ScrollEvent event) -> {
      double zoomFactor = event.getDeltaY() < 0 ? ZOOM_FACTOR * 1.15 : 1 / ZOOM_FACTOR;

      this.getTopLeftCoords().scale(zoomFactor);
      this.getBottomRightCoords().scale(zoomFactor);

      event.consume();
    });
  }

  public Canvas getCanvasCopy() {
    return canvasCopy;
  }

  @Override
  protected void onMousePressed(MouseEvent event) {
    getCanvasCopy().getGraphicsContext2D().drawImage(this.getGraphicsContext2D().getCanvas().snapshot(null, null), 0, 0);

    // Set the new last mouse position
    super.setLastMousePosX(event.getX());
    super.setLastMousePosY(event.getY());
  }

  @Override
  protected void onMouseDragged(MouseEvent event) {
    this.getGraphicsContext2D().drawImage(getCanvasCopy().getGraphicsContext2D().getCanvas().snapshot(null, null), 0, 0);

    // Set the color
    super.getGraphicsContext2D().setFill(new Color(1, 1, 1, 1));

    double[] topLeftAndSize = getTopLeftAndSize(super.getLastMousePosX(), super.getLastMousePosY(), event.getX(), event.getY());

    double topLeftX = topLeftAndSize[0];
    double topLeftY = topLeftAndSize[1];
    double size = topLeftAndSize[2];

    int lineThickness = 3;

    super.getGraphicsContext2D().setLineWidth(lineThickness);
    super.getGraphicsContext2D().strokeRect(topLeftX, topLeftY, size, size);
  }

  @Override
  protected void onMouseRelease(MouseEvent event) {
    if (event.getButton() == MouseButton.PRIMARY) {
      // Set the corners of the new zoom area

      double[] topLeftAndSize = getTopLeftAndSize(super.getLastMousePosX(), super.getLastMousePosY(), event.getX(), event.getY());

      double topLeftX = topLeftAndSize[0];
      double topLeftY = topLeftAndSize[1];
      double size = topLeftAndSize[2];

      double bottomRightX = topLeftX + size;
      double bottomRightY = topLeftY + size;

      // Convert to plane coordinates
      Vector2D topLeft = pixelCoordsToPlaneCoords(new Vector2D(topLeftX, topLeftY));
      Vector2D bottomRight = pixelCoordsToPlaneCoords(new Vector2D(bottomRightX, bottomRightY));

      // Set the new zoom
      this.setNewZoom(topLeft, bottomRight);
    } else if (event.getButton() == MouseButton.SECONDARY) {
      this.rollbackZoom();
    }
  }

  private double[] getTopLeftAndSize(double x1, double y1, double x2, double y2) {
    double dx = x2 - x1;
    double dy = y2 - y1;

    double size = Math.max(Math.abs(dx), Math.abs(dy));

    double topLeftX = (dx > 0) ? x1 : x1 - size;
    double topLeftY = (dy > 0) ? y1 : y1 - size;

    return new double[]{(int) topLeftX, (int) topLeftY, (int) size};
  }

  private void setNewZoom(Vector2D topLeftCoords, Vector2D bottomRightCoords) {
    zoomLog.add(new Vector2D[]{topLeftCoords, bottomRightCoords});
    super.setTopLeftCoords(topLeftCoords);
    super.setBottomRightCoords(bottomRightCoords);
    reCalculateChaosGame();
  }

  private void rollbackZoom() {
    if (!zoomLog.isEmpty()) {
      zoomLog.removeLast();
      super.setTopLeftCoords(zoomLog.getLast()[0]);
      super.setBottomRightCoords(zoomLog.getLast()[1]);
      reCalculateChaosGame();
    }
  }

  private void reCalculateChaosGame() {
    getObservers().forEach(observer -> observer.update(ObserverMessage.NEW_ZOOM, null));
  }

  @Override
  public ArrayList<Observer> getObservers() {
    return observers;
  }

  @Override
  public void addObserver(Observer observer) {
    getObservers().add(observer);
  }

  @Override
  public void removeObserver(Observer observer) {
    getObservers().remove(observer);
  }

  @Override
  public void notifyObservers(ObserverMessage message, Object[] data) {
    getObservers().forEach(o -> o.update(message, data));
  }
}
