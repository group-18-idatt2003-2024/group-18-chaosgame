package edu.ntnu.idatt2001.group18.chaosgame.controller.cli.commands;

import edu.ntnu.idatt2001.group18.chaosgame.model.ChaosGameDescription;
import edu.ntnu.idatt2001.group18.chaosgame.utils.ChaosGameFileHandler;
import edu.ntnu.idatt2001.group18.chaosgame.utils.CliUtils;

public class ViewFractalCommand extends Command {
  public ViewFractalCommand(String command, String description) {
    super(command, description);
  }

  @Override
  public void execute() {
    ChaosGameDescription description = CliUtils.pickOneFractal(
        "Which fractal would you like to see the description of?"
    );

    if (description == null) {
      return;
    } // is null if user entered 'exit'

    continueWithFractal(description);

    boolean goAgain = CliUtils.yesNo("View another one?");

    if (goAgain) {
      execute();
    }
  }

  public void continueWithFractal(ChaosGameDescription description) {
    System.out.println();
    System.out.println(
        ChaosGameFileHandler.getContentsFromFilename(description.getFileName())
    );
  }
}
