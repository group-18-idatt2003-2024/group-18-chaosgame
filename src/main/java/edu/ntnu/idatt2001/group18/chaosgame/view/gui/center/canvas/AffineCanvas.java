package edu.ntnu.idatt2001.group18.chaosgame.view.gui.center.canvas;

import static edu.ntnu.idatt2001.group18.chaosgame.utils.Constants.ZOOM_FACTOR;

import edu.ntnu.idatt2001.group18.chaosgame.model.math.Vector2D;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;


/**
 * <h1>AffineCanvas</h1>
 * AffineCanvas.
 *
 * <p>
 * This class is a canvas for the affine transformations.
 * It is purely responsible for displaying the affine transformations and handling mouse event.
 * </p>
 */
public class AffineCanvas extends ChaosGameCanvas {
  private final ScrollPane parentScrollPane;

  /**
   * <h2>Constructor</h2>
   * Constructor.
   *
   * @param width             The width of the canvas.
   * @param height            The height of the canvas.
   * @param topLeftCoords     The top left plane coordinates of the canvas.
   * @param bottomRightCoords The bottom right plane coordinates of the canvas.
   * @param parentScrollPane  The parent scroll pane (needed for panning).
   */
  public AffineCanvas(
      int width,
      int height,
      Vector2D topLeftCoords,
      Vector2D bottomRightCoords,
      ScrollPane parentScrollPane) {
    super(width, height, topLeftCoords, bottomRightCoords);
    this.parentScrollPane = parentScrollPane;
    setOnMouseScroll();
  }

  public ScrollPane getParentScrollPane() {
    return parentScrollPane;
  }

  @Override
  protected void onMouseDragged(MouseEvent event) {
    // Calculate the change in x and y
    double deltaX = event.getSceneX() - super.getLastMousePosX();
    double deltaY = event.getSceneY() - super.getLastMousePosY();

    // Move the scroll pane
    getParentScrollPane().setHvalue(getParentScrollPane().getHvalue() - deltaX / this.getWidth());
    getParentScrollPane().setVvalue(getParentScrollPane().getVvalue() - deltaY / this.getHeight());

    // Set the new last mouse position
    super.setLastMousePosX(event.getSceneX());
    super.setLastMousePosY(event.getSceneY());
  }

  @Override
  protected void onMousePressed(MouseEvent event) {
    // Set the new last mouse position
    super.setLastMousePosX(event.getSceneX());
    super.setLastMousePosY(event.getSceneY());
  }

  @Override
  protected void onMouseRelease(MouseEvent event) {
  }

  private void setOnMouseScroll() {
    this.setOnScroll((ScrollEvent event) -> {
      // Calculate the zoom factor
      double zoomFactor = event.getDeltaY() > 0 ? ZOOM_FACTOR * 1.15 : 1 / ZOOM_FACTOR;

      // Calculate the new scale
      double newScaleX = this.getScaleX() * zoomFactor;
      double newScaleY = this.getScaleY() * zoomFactor;

      // Set the new scale
      this.setScaleX(newScaleX);
      this.setScaleY(newScaleY);

      // Calculate the distance from the center
      double distanceFromCenterX = this.getWidth() / 2.0 - event.getX();
      double distanceFromCenterY = this.getHeight() / 2.0 - event.getY();

      // Move the canvas to the new position to zoom where the mouse is
      this.setTranslateX(
          this.getTranslateX() + distanceFromCenterX * (1 - 1 / zoomFactor) * this.getScaleX()
      );
      this.setTranslateY(
          this.getTranslateY() + distanceFromCenterY * (1 - 1 / zoomFactor) * this.getScaleY()
      );

      // Consume the event
      event.consume();
    });
  }
}
