package edu.ntnu.idatt2001.group18.chaosgame.view.gui;

import edu.ntnu.idatt2001.group18.chaosgame.utils.ObserverMessage;

import java.util.ArrayList;

/**
 * <h2>Observable</h2>
 * Observable.
 *
 * <p>
 * This interface is used to represent an observable object. The observable object can have
 * observers that are notified when the observable object changes. Implementing classes should
 * have an ArrayList of observers and methods for adding, removing and notifying observers.
 */
public interface Observable {
  ArrayList<Observer> getObservers();

  void addObserver(Observer observer);

  void removeObserver(Observer observer);

  void notifyObservers(ObserverMessage message, Object[] data);
}
