package edu.ntnu.idatt2001.group18.chaosgame.view.gui;

import edu.ntnu.idatt2001.group18.chaosgame.utils.ObserverMessage;

/**
 * <h2>Observer</h2>
 * Observer.
 *
 * <p>
 * This interface is used to represent an observer object. The observer object can be notified by
 * an observable object when the observable object changes.
 * </p>
 */
public interface Observer {
  void update(ObserverMessage message, Object[] data);
}
