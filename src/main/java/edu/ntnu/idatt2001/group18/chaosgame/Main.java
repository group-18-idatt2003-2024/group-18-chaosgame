package edu.ntnu.idatt2001.group18.chaosgame;

import edu.ntnu.idatt2001.group18.chaosgame.controller.cli.Cli;
import edu.ntnu.idatt2001.group18.chaosgame.controller.gui.ChaosGameController;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;

import java.util.Optional;

public class Main extends Application {
  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage stage) throws Exception {
    System.out.println("Starting application...");

    Alert CliOrGuiDialog = new Alert(
        Alert.AlertType.NONE,
        "To GUI, or not to GUI, that is the question...\n\n"
            + "Would you like to launch the application in GUI mode?"
            + "\nNOTE: using CLI in an IDE is not recommended. Please use a proper terminal for CLI.",
        ButtonType.NO,
        ButtonType.YES
    );

    Optional<ButtonType> result = CliOrGuiDialog.showAndWait();

    if (result.isPresent()) {
      if (result.get() == ButtonType.NO) {
        Cli cli = new Cli();
        cli.start();
      } else {
        launchGui(stage);
      }
    }
  }

  private void launchGui(Stage stage) {
    System.out.println("Launching GUI...");

    ChaosGameController controller = new ChaosGameController();

    stage.setMaximized(true);

    stage.setScene(new Scene(controller, 1200, 600));
    stage.show();
  }
}
