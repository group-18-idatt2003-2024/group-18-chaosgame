package edu.ntnu.idatt2001.group18.chaosgame.view.gui.sidebar.settings.editors;

import javafx.scene.Node;

public interface TransformationEditor {
  Node getView();

  void loadTransformation(String data);

  String saveTransformation();
}
