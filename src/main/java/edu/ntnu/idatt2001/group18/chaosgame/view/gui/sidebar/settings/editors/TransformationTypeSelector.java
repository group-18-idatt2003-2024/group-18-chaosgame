package edu.ntnu.idatt2001.group18.chaosgame.view.gui.sidebar.settings.editors;

import edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation.AffineTransform2D;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation.JuliaTransform;
import edu.ntnu.idatt2001.group18.chaosgame.utils.ObserverMessage;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.Observable;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.Observer;
import javafx.scene.Node;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.HBox;

import java.util.ArrayList;

public class TransformationTypeSelector implements Observable {
  private final ArrayList<Observer> observers = new ArrayList<>();
  private final ToggleGroup toggleGroup = new ToggleGroup();
  private ToggleButton affineButton;
  private ToggleButton juliaButton;
  private HBox buttonBox;
  private String editorType;

  public TransformationTypeSelector(String editorType) {
    initializeUi();
    setType(editorType);
  }

  private void initializeUi() {
    affineButton = new ToggleButton(AffineTransform2D.class.getSimpleName());
    affineButton.setToggleGroup(toggleGroup);

    juliaButton = new ToggleButton(JuliaTransform.class.getSimpleName());
    juliaButton.setToggleGroup(toggleGroup);

    buttonBox = new HBox(affineButton, juliaButton);
    buttonBox.setSpacing(10);

    toggleGroup.selectedToggleProperty().addListener(e -> {
      if (toggleGroup.getSelectedToggle() != null) {
        if (!((ToggleButton) toggleGroup.getSelectedToggle()).getText().equals(editorType)) {
          notifyObservers(ObserverMessage.CHANGE_EDITOR,
              new Object[]{((ToggleButton) toggleGroup.getSelectedToggle()).getText()});
        }
      }
      setType(editorType);
    });
  }

  public Node getView() {
    return buttonBox;
  }

  public void setType(String type) {
    editorType = type;
    if (AffineTransform2D.class.getSimpleName().equals(type.trim())) {
      affineButton.setSelected(true);
    } else if (JuliaTransform.class.getSimpleName().equals(type.trim())) {
      juliaButton.setSelected(true);
    }
  }

  @Override
  public ArrayList<Observer> getObservers() {
    return observers;
  }

  @Override
  public void addObserver(Observer observer) {
    getObservers().add(observer);
  }

  @Override
  public void removeObserver(Observer observer) {
    getObservers().remove(observer);
  }

  @Override
  public void notifyObservers(ObserverMessage message, Object[] data) {
    getObservers().forEach(o -> o.update(message, data));
  }
}
