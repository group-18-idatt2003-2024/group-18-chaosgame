package edu.ntnu.idatt2001.group18.chaosgame.model.math;

import edu.ntnu.idatt2001.group18.chaosgame.model.Serializable;

/**
 * <h2>
 * Vector2D
 * </h2>
 * Class to represent a vector in 2D-space.
 *
 * <p>
 * This class represents a vector in 2D-space. The fields represent the x and y
 * coordinates.
 * This class implements getters for the x and y coordinates as well as methods
 * for adding and
 * subtracting another vector.
 * </p>
 */
public class Vector2D implements Serializable {
  private double x0;
  private double x1;

  public Vector2D(double x0, double x1) {
    this.x0 = x0;
    this.x1 = x1;
  }

  /**
   * <h2>
   * fromSerialized
   * </h2>
   * fromSerialized.
   *
   * <p>
   * This method returns an instance of the Vector2D-class from a given serialized string.
   * The string must be in the format "x,y".
   * </p>
   *
   * @param serialized The raw string-serial to create an instance from.
   * @return The instance created from a serial.
   */
  public static Vector2D fromSerialized(String serialized) {
    String[] parts = serialized.split(",");

    return new Vector2D(
        Double.parseDouble(parts[0]),
        Double.parseDouble(parts[1])
    );
  }

  public double getX0() {
    return x0;
  }

  public void setX0(double newX0) {
    x0 = newX0;
  }

  public double getX1() {
    return x1;
  }

  public void setX1(double newX1) {
    x1 = newX1;
  }

  public Vector2D add(Vector2D other) {
    return new Vector2D(x0 + other.x0, x1 + other.x1);
  }

  public Vector2D subtract(Vector2D other) {
    if (isNotSameType(other)) {
      throw new IllegalArgumentException(
          String.format(
              "The vectors must be of the same type. Cannot subtract '%s' from '%s'%n",
              other.getClass().getSimpleName(),
              this.getClass().getSimpleName()
          )
      );
    }

    return new Vector2D(x0 - other.x0, x1 - other.x1);
  }

  public Vector2D multiply(Vector2D other) {
    if (isNotSameType(other)) {
      throw new IllegalArgumentException(
          String.format(
              "The vectors must be of the same type. Cannot multiply '%s' with '%s'%n",
              other.getClass().getSimpleName(),
              this.getClass().getSimpleName()
          )
      );
    }

    return new Vector2D(x0 * other.x0, x1 * other.x1);
  }

  public Vector2D scale(double scalar) {
    return new Vector2D(x0 * scalar, x1 * scalar);
  }

  private boolean isNotSameType(Vector2D other) {
    return !this.getClass().equals(other.getClass());
  }

  @Override
  public String serialize() {
    return x0 + "," + x1;
  }
}
