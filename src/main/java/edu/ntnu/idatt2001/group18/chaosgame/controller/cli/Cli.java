package edu.ntnu.idatt2001.group18.chaosgame.controller.cli;

import edu.ntnu.idatt2001.group18.chaosgame.controller.cli.commands.Command;
import edu.ntnu.idatt2001.group18.chaosgame.controller.cli.commands.CreateNewFractal;
import edu.ntnu.idatt2001.group18.chaosgame.controller.cli.commands.DeleteFractal;
import edu.ntnu.idatt2001.group18.chaosgame.controller.cli.commands.DrawFractalCommand;
import edu.ntnu.idatt2001.group18.chaosgame.controller.cli.commands.ViewFractalCommand;
import edu.ntnu.idatt2001.group18.chaosgame.utils.CliUtils;
import edu.ntnu.idatt2001.group18.chaosgame.utils.Constants;
import edu.ntnu.idatt2001.group18.chaosgame.view.cli.CliView;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * <h1>Cli</h1>
 * Cli.
 *
 * <p>
 *   This class is the main entry point for the CLI version of the game.
 * </p>
 */
public class Cli {
  private final CliView cliView = new CliView();
  private final List<Command> commandList = new ArrayList<>(List.of(
      new DrawFractalCommand("draw-fractal", "Draws a fractal"),
      new CreateNewFractal("create-fractal", "Create a new fractal and write it to file"),
      new ViewFractalCommand("view-fractal", "View fractal data"),
      new DeleteFractal("delete-fractal", "Delete a fractal")
  ));

  public Cli() {
    super();
    greet();
  }

  /**
   * <h2>Greet</h2>
   * Greet.
   *
   * <p>
   *   Greet the user.
   * </p>
   */
  public void greet() {
    cliView.print("""
        %s
        Hello and Welcome to Chaos Game!
              \s
        This game is based on mathematical fractals.
        You can create and view however many you'd like!
        """, Constants.BANNER);
  }

  /**
   * <h2>Start</h2>
   * Start.
   *
   * <p>
   *   Start the CLI.
   * </p>
   */
  public void start() {
    while (true) {
      System.out.println();
      commandList.forEach((command -> cliView.print(
          "%s - %s",
          command.getCommand(),
          command.getDescription())));

      cliView.print("\nType 'exit' to exit the game.");
      cliView.print("Write a command:");
      String userInput = cliView.getInput("> ");

      if (userInput.equals("exit")) {
        System.out.println(Constants.GOODBYE);
        System.exit(0);
      }

      Optional<Command> commandToExecute = commandList.stream()
          .filter(command -> command.getCommand().contains(userInput))
          .findFirst();

      commandToExecute.ifPresentOrElse(
          Command::execute,
          () -> {
            System.err.printf("Command '%s' not found%n", userInput);
            CliUtils.pressEnterToContinue();
          });
    }
  }
}
