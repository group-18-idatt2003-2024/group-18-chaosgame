package edu.ntnu.idatt2001.group18.chaosgame.view.gui.sidebar.settings;

import edu.ntnu.idatt2001.group18.chaosgame.utils.ObserverMessage;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.Observable;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.Observer;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StepsView extends GridPane implements Observable {
  private final ArrayList<Observer> observers = new ArrayList<>();
  private final TextField stepCountField = new TextField();
  private final Button runButton = new Button("Run");

  /**
   * <h2>Constructor</h2>
   * Constructor.
   *
   * <p>
   * Constructor for the StepsView class. This class is the view for the steps settings.
   * </p>
   */
  public StepsView() {
    super();

    setHgap(10);

    Label stepLabel = new Label("Steps:");
    stepLabel.setStyle("-fx-font-weight: bold;");
    setupStepCountField();
    setupRunStepsButton();

    prefWidthProperty().setValue(Double.MAX_VALUE);
    stepLabel.minWidth(80);
    stepCountField.prefWidthProperty().bind(widthProperty());
    runButton.prefWidthProperty().bind(widthProperty().divide(2.5));

    runButton.setStyle("-fx-background-color: #44aa44; -fx-text-fill: white; -fx-font-weight: bold;"
        + " -fx-effect: dropshadow(three-pass-box, rgba(0.2,0.2,0.2,0.3), 5, 0, 0, 2);");
    runButton.setOnMouseEntered(e -> runButton.setStyle("-fx-cursor: hand; -fx-scale-x: 1.02; -fx-scale-y: 1.02;"
        + "-fx-background-color: #55bb55; -fx-text-fill: white; -fx-font-weight: bold; "
        + "-fx-effect: " + "dropshadow(three-pass-box, rgba(0.2,0.2,0.2,0.3), 5, 0, 0, 2);"));
    runButton.setOnMouseExited(e -> runButton.setStyle("-fx-background-color: #44aa44; -fx-text-fill: white; "
        + "-fx-font-weight: bold; -fx-effect: dropshadow(three-pass-box, rgba(0.2,0.2,0.2,0.3), "
        + "5, 0, 0, 2);"));

    addRow(0, stepLabel, stepCountField, runButton);
  }

  private TextField getStepCountField() {
    return stepCountField;
  }

  public Button getRunStepsButton() {
    return runButton;
  }

  public int getStepCount() {
    // If the step count field is not empty, return the value of the field, else return 1
    if (!getStepCountField().getText().isBlank()) {
      return Integer.parseInt(getStepCountField().getText());
    } else {
      getStepCountField().setText("1000000");
      return getStepCount();
    }
  }

  private void setupRunStepsButton() {
    // Notify the observers when the run steps button is clicked with the step count as data
    getRunStepsButton().setOnAction(e -> {
      notifyObservers(ObserverMessage.RUN_GAME, new Object[]{getStepCount()});
    });
  }

  private void setupStepCountField() {
    getStepCountField().setMaxWidth(100);
    getStepCountField().setPromptText("Steps to run");
    getStepCountField().setText("100000");

    // Limit the step count field from 1 to 1_000_000
    getStepCountField().textProperty().addListener((observable, oldValue, newValue) -> {
      if (isInteger(newValue)) {
        if (Integer.parseInt(newValue) > 3_000_000) {
          getStepCountField().setText("3000000");
        } else if (Integer.parseInt(newValue) < 1) {
          getStepCountField().setText("1");
        }
      } else if (!newValue.isBlank()) {
        getStepCountField().setText(oldValue);
      }
    });
  }

  private boolean isInteger(String input) {
    String regex = "^-?\\d+$";
    Pattern pattern = Pattern.compile(regex);
    Matcher matcher = pattern.matcher(input);
    return matcher.matches();
  }

  public void toggleStepVisibility(boolean visible) {
    getStepCountField().setDisable(!visible);
  }

  @Override
  public ArrayList<Observer> getObservers() {
    return observers;
  }

  @Override
  public void addObserver(Observer observer) {
    getObservers().add(observer);
    notifyObservers(ObserverMessage.RUN_GAME, new Object[]{1});
  }

  @Override
  public void removeObserver(Observer observer) {
    getObservers().remove(observer);
  }

  @Override
  public void notifyObservers(ObserverMessage message, Object[] data) {
    getObservers().forEach(o -> o.update(message, data));
  }
}
