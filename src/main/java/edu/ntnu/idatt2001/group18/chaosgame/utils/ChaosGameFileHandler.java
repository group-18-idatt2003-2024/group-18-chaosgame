package edu.ntnu.idatt2001.group18.chaosgame.utils;

import edu.ntnu.idatt2001.group18.chaosgame.model.ChaosGameDescription;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.Vector2D;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation.AffineTransform2D;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation.JuliaTransform;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation.Transform2D;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;


/**
 * <h1>ChaosGameFileHandler</h1>
 * ChaosGameFileHandler.
 *
 * <p>
 * Class for handling saved fractal-descriptions.
 * </p>
 */
public class ChaosGameFileHandler {
  /**
   * <h2>readFromFile</h2>
   * readFromFile.
   *
   * <p>
   * Method that takes a file as an argument and returns a fractal-description based on the
   * contents of the given file.
   * </p>
   *
   * @param file The file to get the description from.
   * @return The description generated from the file.
   */
  public static ChaosGameDescription readFromFile(File file) {
    // Get the contents of the file
    String fileContents = getFileContents(file);

    // Split the contents of the file into an array of lines
    assert fileContents != null;
    String[] fileLines = fileContents.split("\n");

    // The first line is the type of transformation
    String transformationType = fileLines[0];

    // The second and third lines are the min and max coordinates respectively
    double[] minCoordsArray = Arrays
        .stream(fileLines[1].split(","))
        .mapToDouble(Double::parseDouble)
        .toArray();
    double[] maxCoordsArray = Arrays
        .stream(fileLines[2].split(","))
        .mapToDouble(Double::parseDouble)
        .toArray();

    Vector2D minCoords = new Vector2D(
        minCoordsArray[0],
        minCoordsArray[1]
    );
    Vector2D maxCoords = new Vector2D(
        maxCoordsArray[0],
        maxCoordsArray[1]
    );

    ArrayList<Transform2D> transforms = new ArrayList<>();

    // The rest of the lines are the transformations
    for (int currentLine = 3; currentLine < fileLines.length; currentLine++) {
      if (transformationType.equals(AffineTransform2D.class.getSimpleName())) {
        transforms.add(
            AffineTransform2D.fromSerial(fileLines[currentLine])
        );
      } else if (transformationType.equals(JuliaTransform.class.getSimpleName())) {
        transforms.add(
            JuliaTransform.fromSerial(fileLines[currentLine])
        );
      }
    }

    // Return the description
    return new ChaosGameDescription(
        file.getName(),
        transforms,
        minCoords.getX0(),
        maxCoords.getX0(),
        minCoords.getX1()
    );
  }

  /**
   * <h2>writeToFile</h2>
   * writeToFile.
   *
   * <p>
   * Method-used for writing a given description to a file.
   * </p>
   *
   * @param description The description to write to a file.
   */
  public static void writeToFile(ChaosGameDescription description) {
    // Generate the file path and create the file
    String filepath = Constants.FRACTAL_FOLDER_PATH + description.getFileName();
    File descriptionFile = new File(filepath);

    // Write the description to the file
    try {
      // Check if the file already exists and ask the user if they want to overwrite it
      if (descriptionFile.exists()) {
        boolean shouldOverwrite = CliUtils.yesNo(
            String.format(
                "The file '%s' already exists. Would you like to overwrite?",
                description.getFileName()
            )
        );
        if (shouldOverwrite) {
          descriptionFile.delete();
        } else {
          return;
        }
      } else {
        descriptionFile.createNewFile();
      }

      System.out.println("File for description created at '" + description.getFileName() + "'");

      StringBuilder descriptionText = new StringBuilder();

      // Write all the description data
      descriptionText.append(
          description
              .getTransforms()
              .getFirst()
              .getClass()
              .getSimpleName()
      ).append("\n");

      descriptionText.append(description.getMinCoords().serialize()).append("\n");
      descriptionText.append(description.getMaxCoords().serialize()).append("\n");

      for (Transform2D transform : description.getTransforms()) {
        descriptionText.append(transform.serialize());
        descriptionText.append("\n");
      }

      FileWriter fileWriter = new FileWriter(filepath);
      fileWriter.write(descriptionText.toString());
      fileWriter.close();
    } catch (IOException e) {
      System.err.println(e.getMessage());
    }
  }

  /**
   * <h2>getFractals</h2>
   * getFractals.
   *
   * <p>
   * The method used to get a list of all the stored descriptions in the descriptions folder.
   * </p>
   *
   * @return The list of descriptions.
   */
  public static ChaosGameDescription[] getFractals() {
    File fractalFolder = new File(Constants.FRACTAL_FOLDER_PATH);
    File[] listOfFiles = fractalFolder.listFiles();
    ChaosGameDescription[] descriptions = new ChaosGameDescription[listOfFiles.length];

    for (int i = 0; i < listOfFiles.length; i++) {
      descriptions[i] = readFromFile(listOfFiles[i]);
    }

    return descriptions;
  }

  /**
   * <h2>getFileContents</h2>
   * getFileContents.
   *
   * <p>
   * Method used for getting the raw file contents of a given file.
   * </p>
   *
   * @param file The file to get the contents of.
   * @return The contents of the file.
   */
  public static String getFileContents(File file) {
    try {
      if (file.canRead()) {
        // Read all lines of the file and put it in one string
        BufferedReader reader = new BufferedReader(new FileReader(file));
        StringBuilder descriptionText = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
          descriptionText.append(line).append("\n");
        }
        return descriptionText.toString();
      } else {
        return null;
      }
    } catch (IOException e) {
      System.err.println(e.getMessage());
      return null;
    }
  }

  public static int getFractalCount() {
    return getFractals().length;
  }

  public static File getFileFromName(String filename) {
    return new File(Constants.FRACTAL_FOLDER_PATH + filename);
  }


  /**
   * <h2>getTransformationType</h2>
   * getTransformationType.
   *
   * <p>
   *   Method used for getting the transformation type of a given file.
   * </p>
   *
   * @param file  The file to get the transformation type of.
   * @return    The transformation type.
   */
  public static String getTransformationType(File file) {
    try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
      String type = reader.readLine();
      if (type != null) {
        return type;
      }
      return "Unknown";
    } catch (IOException e) {
      System.err.println("Error reading from file: " + e.getMessage());
    }
    return "Unknown";
  }

  public static String getContentsFromFilename(String filename) {
    return getFileContents(getFileFromName(filename));
  }
}
