package edu.ntnu.idatt2001.group18.chaosgame.view.cli;

import java.util.Scanner;

public class CliView {
  private final Scanner sc = new Scanner(System.in);

  private Scanner getSc() {
    return sc;
  }

  public String getInput(String prompt, String... values) {
    System.out.printf(prompt, (Object[]) values);
    String response = getSc().nextLine();
    System.out.println();
    return response;
  }

  public void print(String prompt, String... values) {
    System.out.printf(prompt + "\n", (Object[]) values);
  }
}
