package edu.ntnu.idatt2001.group18.chaosgame.model;

import edu.ntnu.idatt2001.group18.chaosgame.model.math.Vector2D;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation.Transform2D;
import java.util.List;

/**
 * <h1>ChaosGameDescription</h1>
 * ChaosGameDescription.
 *
 * <p>
 * Class that represents a description of a chaos game. It contains a list of
 * transformations, and the minimum and maximum coordinates of the game.
 * </p>
 */
public class ChaosGameDescription {
  private final String fileName;
  private final List<Transform2D> transforms;
  private Vector2D minCoords;
  private Vector2D maxCoords;

  /**
   * <h2>Constructor</h2>
   * Constructor.
   *
   * @param transforms The list of transformations the description has
   * @param xmin       The (often approximate) smallest x coordinate that the description can have.
   * @param xmax       The (often approximate) largest x coordinate that the description can have.
   * @param ymin       The (often approximate) smallest y coordinate that the description can have.
   *                   The largest y min value is automatically calculated based on the other
   *                   values. This is done to ensure that the aspect ratio of the game is 1:1.
   */
  public ChaosGameDescription(String filename,
                              List<Transform2D> transforms,
                              double xmin,
                              double xmax,
                              double ymin) {

    if (!filename.endsWith(".txt")) {
      filename += ".txt";
    }

    this.minCoords = new Vector2D(xmin, ymin);
    this.maxCoords = new Vector2D(xmax, ymin + xmax - xmin);
    this.transforms = transforms;
    this.fileName = filename;
  }

  public String getFileName() {
    return fileName;
  }

  public List<Transform2D> getTransforms() {
    return transforms;
  }

  public Vector2D getMinCoords() {
    return minCoords;
  }

  public void setMinCoords(Vector2D newMinCoords) {
    minCoords = newMinCoords;
  }

  public Vector2D getMaxCoords() {
    return maxCoords;
  }

  public void setMaxCoords(Vector2D maxCoords) {
    this.maxCoords = maxCoords;
  }
}
