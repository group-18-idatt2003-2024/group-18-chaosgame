package edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation;

import edu.ntnu.idatt2001.group18.chaosgame.model.math.Complex;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.Vector2D;

import java.util.Arrays;
import java.util.Random;

/**
 * <h2>
 * JuliaTransform class
 * </h2>
 * Class for Julia transformations.
 *
 * <p>
 * This class is used to represent a Julia transformation in 2D space.
 * It implements the transform method.
 * </p>
 */
public class JuliaTransform extends Transform2D {
  private final Complex point;
  private final int sign;

  /**
   * Constructor for the JuliaTransform class.
   *
   * @param point The point to be transformed around.
   * @param sign  The sign of the transformation.
   */
  public JuliaTransform(Complex point, int sign) {
    this.point = point;

    assert sign == 1 || sign == -1 : "Sign must be 1 or -1";
    this.sign = sign;
  }

  public JuliaTransform(Complex point) {
    this.point = point;
    this.sign = 1;
  }

  /**
   * <h2>
   * fromSerial
   * </h2>
   * fromSerial.
   *
   * <p>
   * Method for creating a JuliaTransform object from a serialized string.
   * The string must be in the format "x,y,sign".
   * </p>
   *
   * @param serialString The serialized string to create the object from.
   * @return The JuliaTransform object created from the serialized string.
   */
  public static JuliaTransform fromSerial(String serialString) {
    double[] serial = Arrays
        .stream(serialString.split(","))
        .mapToDouble(Double::parseDouble)
        .toArray();

    Complex point = new Complex(serial[0], serial[1]);
    int sign = (int) serial[2];

    return new JuliaTransform(point, sign);
  }

  /**
   * <h2>
   * transform
   * </h2>
   * transform.
   *
   * <p>
   * This method is used to transform a point using a Julia transformation.
   * It uses a smarter way of calculating for a specific point. This way
   * we can get a color for each pixel.
   * </p>
   *
   * @param point The point to be transformed
   * @return The point after the transformation has been applied
   */
  @Override
  public Vector2D transform(Vector2D point) {
    if (point == null) {
      throw new NullPointerException("Point cannot be null");
    }

    // z = z^2 + c
    Complex z = new Complex(point);
    Complex c = new Complex(this.point);

    return new Complex(z.square().add(c));
  }

  /**
   * <h2>
   * cliTransform
   * </h2>
   * cliTransform.
   *
   * <p>
   * This method is used to transform a point using a Julia transformation.
   * It is only used for cli purposes.
   * </p>
   *
   * @param pointToTransform The point to be transformed
   * @return The point after the transformation has been applied
   */
  public Vector2D cliTransform(Vector2D pointToTransform) {
    Complex complex = new Complex(pointToTransform.getX0(), pointToTransform.getX1());
    Complex subtractedComplex = new Complex(complex.subtract(this.point));
    Complex[] roots = subtractedComplex.sqrt();

    // Pick a random root
    Random random = new Random();
    Complex chosenRoot = roots[random.nextInt(2)];

    // Convert back to Vector2D and return
    return new Vector2D(chosenRoot.getX0(), chosenRoot.getX1());
  }

  @Override
  public String serialize() {
    return point.serialize() + "," + sign;
  }
}
