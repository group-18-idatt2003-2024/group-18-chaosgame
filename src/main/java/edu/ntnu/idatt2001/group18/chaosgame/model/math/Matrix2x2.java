package edu.ntnu.idatt2001.group18.chaosgame.model.math;

import edu.ntnu.idatt2001.group18.chaosgame.model.Serializable;

/**
 * <h2>
 * Matrix2x2
 * </h2>
 * Class to represent a 2x2 matrix.
 *
 * <p>
 * This class represents a 2x2 matrix. The fields represent the four elements of the matrix.
 * This class implements a method for multiplying the matrix with a vector.
 * </p>
 */
public class Matrix2x2 implements Serializable {
  private final double a00;
  private final double a01;
  private final double a10;
  private final double a11;


  /**
   * Constructor for the Matrix2x2 class.
   *
   * @param a00 First element of first row
   * @param a01 Second element of first row
   * @param a10 First element of second row
   * @param a11 Second element of second row
   */
  public Matrix2x2(double a00, double a01, double a10, double a11) {
    this.a00 = a00;
    this.a01 = a01;
    this.a10 = a10;
    this.a11 = a11;
  }

  public double getA00() {
    return a00;
  }

  public double getA01() {
    return a01;
  }

  public double getA10() {
    return a10;
  }

  public double getA11() {
    return a11;
  }

  /**
   * <h2>
   * multiply
   * </h2>
   * multiply.
   *
   * <p>
   * Method for multiplying the matrix with a vector.
   * </p>
   *
   * @param vector The vector to multiply the matrix with.
   * @return The resulting vector.
   */
  public Vector2D multiply(Vector2D vector) {
    if (vector instanceof Complex) {
      throw new IllegalArgumentException("Cannot multiply a matrix with a complex number");
    }

    return new Vector2D(
        a00 * vector.getX0() + a01 * vector.getX1(),
        a10 * vector.getX0() + a11 * vector.getX1()
    );
  }

  @Override
  public String serialize() {
    return a00 + "," + a01 + "," + a10 + "," + a11;
  }
}
