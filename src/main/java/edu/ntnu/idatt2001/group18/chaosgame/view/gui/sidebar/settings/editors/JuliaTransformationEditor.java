package edu.ntnu.idatt2001.group18.chaosgame.view.gui.sidebar.settings.editors;

import edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation.JuliaTransform;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class JuliaTransformationEditor extends BaseTransformationEditor {

  private final TextField vectorX = new TextField();
  private final TextField vectorY = new TextField();

  public JuliaTransformationEditor() {
    super(JuliaTransform.class.getSimpleName());
    setupJuliaSpecificControls();
  }

  private void setupJuliaSpecificControls() {
    Label complexTitle = new Label("Complex:");
    complexTitle.setStyle("-fx-font-weight: bold");
    complexTitle.setPadding(new Insets(20, 0, 0, 0));
    GridPane.setColumnSpan(complexTitle, 4);  // Span across all columns
    view.add(complexTitle, 0, 5);

    Label realLabel = new Label("Real:");
    Label imagLabel = new Label("Imag.:");

    setupRevertOnInvalidInput(vectorX);
    setupRevertOnInvalidInput(vectorY);

    int rowIndex = 6;
    view.addRow(rowIndex, realLabel, vectorX, imagLabel, vectorY);
  }

  @Override
  public void loadTransformation(String data) {
    String[] lines = data.split("\n");
    if (lines.length >= 3) {
      // Load min/max coordinates using the base class method
      this.typeSelector.setType(JuliaTransform.class.getSimpleName());
      super.loadCoordinates(lines[1], lines[2]);
    }
    if (lines.length >= 4) {
      String[] vectorParts = lines[3].split(",");
      if (vectorParts.length >= 2) {
        vectorX.setText(vectorParts[0]);
        vectorY.setText(vectorParts[1]);
      }
    }
  }

  @Override
  public String saveTransformation() {
    // Save the base coordinate string and append the Julia vector details
    if (vectorX.getText().isEmpty()) {
      vectorX.setText("0");
    }
    if (vectorY.getText().isEmpty()) {
      vectorY.setText("0");
    }
    return JuliaTransform.class.getSimpleName() + "\n" + super.getCoordinateString() + "\n"
        + vectorX.getText() + "," + vectorY.getText() + ",1";  // Append '1' as the sign (non-editable)
  }
}
