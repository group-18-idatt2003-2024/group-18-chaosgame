package edu.ntnu.idatt2001.group18.chaosgame.view.gui.sidebar.other;

import edu.ntnu.idatt2001.group18.chaosgame.utils.Constants;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;

public class HelpView extends Button {
  public HelpView() {
    super("Help");

    setPrefWidth(260);
    setStyle("-fx-font-size: 16; -fx-background-color: " + "#9999ee" + "; -fx-text-fill: white; -fx-font-weight: " +
        "bold; -fx-effect: dropshadow(three-pass-box, rgba(0.2,0.2,0.2,0.3), 5, 0, 0, 2);");
    setOnMouseEntered(e -> setStyle("-fx-font-size: 16; -fx-cursor: hand; -fx-scale-x: 1.02; " +
        "-fx-scale-y: 1.02; -fx-background-color: " + "#aaaaff" + "; -fx-text-fill: white; -fx-font-weight: bold; "
        + "-fx-effect: " + "dropshadow(three-pass-box, rgba(0.2,0.2,0.2,0.3), 5, 0, 0, 2);"));
    setOnMouseExited(e -> setStyle("-fx-font-size: 16; -fx-background-color: " + "#9999ee" + "; -fx-text" +
        "-fill: white; -fx-font-weight: bold; -fx-effect: dropshadow(three-pass-box, rgba(0.2,0.2,0.2,0.3), "
        + "5, 0, 0, 2);"));

    this.setOnAction(e -> {
      openHelp();
    });

    openHelp();
  }

  private void openHelp() {
    Alert helpAlert = new Alert(Alert.AlertType.INFORMATION);

    helpAlert.setTitle("Help");
    helpAlert.setHeaderText("Welcome!");

    helpAlert.getDialogPane().setContent(Constants.HELP_LABEL);

    helpAlert.showAndWait();
  }
}
