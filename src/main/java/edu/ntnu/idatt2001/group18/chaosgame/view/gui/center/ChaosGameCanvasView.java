package edu.ntnu.idatt2001.group18.chaosgame.view.gui.center;

import edu.ntnu.idatt2001.group18.chaosgame.model.ChaosGameDescription;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.Vector2D;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation.AffineTransform2D;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation.JuliaTransform;
import edu.ntnu.idatt2001.group18.chaosgame.utils.Constants;
import edu.ntnu.idatt2001.group18.chaosgame.utils.ObserverMessage;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.Observable;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.Observer;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.center.canvas.AffineCanvas;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.center.canvas.ChaosGameCanvas;
import edu.ntnu.idatt2001.group18.chaosgame.view.gui.center.canvas.JuliaCanvas;
import java.util.ArrayList;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * <h2>ChaosGameCanvasView</h2>
 * ChaosGameCanvasView.
 *
 * <p>
 * This class is a view for the canvas of the chaos game.
 * It is responsible for displaying the canvas.
 * </p>
 */
public class ChaosGameCanvasView extends ScrollPane implements Observable, Observer {
  private final ArrayList<Observer> observers = new ArrayList<>();
  private final Region pannableRegion = new Region();
  private final StackPane pane = new StackPane();
  private ChaosGameCanvas canvas;

  /**
   * <h2>Constructor</h2>
   * Constructor.
   *
   * <p>
   * Constructor for the ChaosGameCanvasView class.
   * </p>
   */
  public ChaosGameCanvasView() {
    super();

    // Create a new initial canvas
    canvas = new AffineCanvas(0, 0, new Vector2D(0, 0), new Vector2D(0, 0), this);

    // Create a region so the ScrollPane is always pannable
    pannableRegion.setMinSize(2000, 2000);
    pane.getChildren().addAll(pannableRegion, canvas);

    this.setContent(pane);

    // Set the ScrollPane to be centered
    this.setHvalue(0.5);
    this.setVvalue(0.5);
  }

  public ChaosGameCanvas getCanvas() {
    return canvas;
  }

  /**
   * <h2>setCanvas</h2>
   * setCanvas.
   *
   * <p>
   *   Set the canvas of the ChaosGameCanvasView. It also sets the scroll values to center.
   * </p>
   *
   * @param newCanvas The new canvas to set.
   */
  public void setCanvas(ChaosGameCanvas newCanvas) {
    // Remove the old canvas and add the new canvas
    getPane().getChildren().removeAll(getCanvas(), pannableRegion);
    this.canvas = newCanvas;
    getPane().getChildren().addAll(pannableRegion, getCanvas());

    // Force another layout pass (fixes centering on new canvas)y
    getPane().layout();
    this.layout();

    // Set the scroll values to center
    setHvalue(0.5);
    setVvalue(0.5);
  }

  private StackPane getPane() {
    return pane;
  }

  /**
   * <h2>createCanvasFromDescription</h2>
   * createCanvasFromDescription.
   *
   * <p>
   * Create a canvas from a description. Which canvas to create depends on
   * whether it's julia or affine transformations.
   * </p>
   *
   * @param description The description to create a canvas for.
   */
  public void createCanvasFromDescription(ChaosGameDescription description) {
    if (description.getTransforms().getFirst() instanceof AffineTransform2D) {
      this.setCanvas(ChaosGameCanvas.createCanvasForAffineGame(
          description.getMinCoords(),
          description.getMaxCoords(),
          this,
          500,
          500
      ));
    } else if (description.getTransforms().getFirst() instanceof JuliaTransform) {
      this.setCanvas(ChaosGameCanvas.createCanvasForJuliaGame(
          description.getMinCoords(),
          description.getMaxCoords()
      ));
    } else {
      throw new IllegalArgumentException(
          String.format("No canvas has been implemented for transformation type %s%n",
          description.getTransforms().getFirst()
          )
      );
    }

    notifyObservers(ObserverMessage.NEW_CANVAS, new Object[]{getCanvas().getHeight()});

    // Add this as an observer to the canvas if it's a julia canvas (needed for zoom re-rendering)
    if (getCanvas() instanceof JuliaCanvas) {
      ((JuliaCanvas) getCanvas()).addObserver(this);
    }
  }

  /**
   * <h2>drawPixelFromPixelCoords</h2>
   * drawPixelFromPixelCoords.
   *
   * <p>
   *   Draw a pixel on the canvas from pixel coordinates.
   * </p>
   *
   * @param coords  The coordinates of the pixel.
   * @param color  The color of the pixel.
   */
  public void drawPixelFromPixelCoords(Vector2D coords, Color color) {
    getCanvas().getGraphicsContext2D().getPixelWriter().setColor(
        (int) coords.getX0(),
        (int) coords.getX1(),
        color
    );
  }

  @Override
  public void update(ObserverMessage message, Object[] data) {
    switch (message) {
      case ObserverMessage.NEW_POSITION:
        // data = {Vector2D coords, Integer iterations}
        // Cast the data to the correct types
        final Vector2D coords = (Vector2D) data[0];
        final Integer iterations = (Integer) data[1];
        Color color;
        // Set the color of the pixel based on the amount of iterations
        // Affine transforms are just black.
        if (iterations != 0 && iterations != Constants.JULIA_MAX_ITERATIONS + 1) {
          color = getColorFromIterations(iterations);
        } else {
          color = Color.BLACK;
        }
        // Draw the pixel
        drawPixelFromPixelCoords(coords, color);
        break;

      case ObserverMessage.NEW_ZOOM:
        // data = {}
        // Update controller to re-run the game
        observers.forEach(observer -> observer.update(ObserverMessage.NEW_ZOOM, null));
        break;

      case ObserverMessage.NEW_CANVAS:
        // data = {int size}
        // New canvas can only be created for affine canvases
        if (getCanvas() instanceof AffineCanvas) {
          setCanvas(new AffineCanvas(
              (int) data[0],
              (int) data[0],
              getCanvas().getTopLeftCoords(),
              getCanvas().getBottomRightCoords(),
              this
          ));
        }
        break;
      case ObserverMessage.LOADING:
        System.out.println("Drawing loading");
        final int fontSize = (int) (getCanvas().getHeight() / 10);
        Font font = new Font("Arial", fontSize);
        getCanvas().getGraphicsContext2D().setFont(font);
        getCanvas().getGraphicsContext2D().setFill(Color.BLACK);

        double x = getCanvas().getWidth() / 4.5;
        double y = (getCanvas().getHeight() + fontSize) / 2;

        getCanvas().getGraphicsContext2D().fillText("LOADING...", x, y);
        break;

      default:
        throw new IllegalArgumentException(String.format("Message %s not recognized%n", message));
    }
  }

  // Get the color of a pixel based on the amount of iterations
  private Color getColorFromIterations(int iterations) {
    int spacing = Constants.JULIA_MAX_ITERATIONS / 2;
    double hue = (double) (360 * (iterations % spacing)) / spacing;
    return Color.hsb(hue, 1.0, 1.0);
  }

  @Override
  public ArrayList<Observer> getObservers() {
    return observers;
  }

  @Override
  public void addObserver(Observer observer) {
    if (!getObservers().contains(observer)) {
      getObservers().add(observer);
    }
  }

  @Override
  public void removeObserver(Observer observer) {
    getObservers().remove(observer);
  }

  @Override
  public void notifyObservers(ObserverMessage message, Object[] data) {
    getObservers().forEach(o -> o.update(message, data));
  }
}
