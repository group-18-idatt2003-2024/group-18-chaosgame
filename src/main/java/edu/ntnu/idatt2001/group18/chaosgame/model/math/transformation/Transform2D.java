package edu.ntnu.idatt2001.group18.chaosgame.model.math.transformation;

import edu.ntnu.idatt2001.group18.chaosgame.model.Serializable;
import edu.ntnu.idatt2001.group18.chaosgame.model.math.Vector2D;

/**
 * <h2>Transform2D</h2>
 * Transform2D.
 *
 * <p>
 * This class is used to represent a 2D transformation. It is an abstract
 * class, and is meant to be extended by classes representing specific
 * transformations.
 * </p>
 */
public abstract class Transform2D implements Serializable {
  public abstract Vector2D transform(Vector2D point);
}
